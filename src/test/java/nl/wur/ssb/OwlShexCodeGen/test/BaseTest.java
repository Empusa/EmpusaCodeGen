package nl.wur.ssb.OwlShexCodeGen.test;

import junit.framework.TestCase;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.ListIndexException;
import org.empusa.test.domain.EnumExample;
import org.empusa.test.domain.Example;
import org.empusa.test.domain.TestA;
import org.empusa.test.domain.impl.TestAImpl;
import org.junit.Assert;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

public class BaseTest extends TestCase {
    public void test1() throws Exception {
        Domain domain = new Domain(new RDFSimpleCon(""));
        Example example1 = domain.make(Example.class, "http://example.com/test");
        Assert.assertSame(example1, domain.make(Example.class, "http://example.com/test"));
        Assert.assertNull(example1.getProp0_1());
        example1.setProp0_1("test1");
        Assert.assertEquals("test1", example1.getProp0_1());
        try {
            example1.getProp1_1();
            Assert.fail();
        } catch (RuntimeException ignored) {
        }
        example1.setProp1_1("test2");
        Assert.assertEquals("test2", example1.getProp1_1());
        example1.setProp1_1("test3");
        example1.setProp1_1("test1");
        example1.setProp1_1("aaa");
        example1.setProp1_1("zzz");
        example1.setProp1_1("test2");
        Assert.assertEquals("test2", example1.getProp1_1());
        Assert.assertEquals("test1", example1.getProp0_1());
        Assert.assertEquals(0, example1.getAllProp0_N().size());
        example1.addProp0_N("testVal1");
        example1.addProp0_N("testVal2");
        String tmp[] = example1.getAllProp0_N().toArray(new String[0]);
        Arrays.sort(tmp);
        Assert.assertEquals(2, tmp.length);
        Assert.assertEquals("testVal1", tmp[0]);
        Assert.assertEquals("testVal2", tmp[1]);
        example1.remProp0_N("testVal1");
        tmp = example1.getAllProp0_N().toArray(new String[0]);
        Assert.assertEquals(1, tmp.length);
        Assert.assertEquals("testVal2", tmp[0]);
        try {
            example1.remProp0_N("testVal1");
            Assert.fail();
        } catch (RuntimeException ignored) {

        }
        example1.remProp0_N("testVal2");
        Assert.assertEquals(0, example1.getAllProp0_N().size());

        try {
            example1.getAllProp1_N();
            Assert.fail();
        } catch (RuntimeException ignored) {
        }
        example1.addProp1_N("testNVal1");
        example1.addProp1_N("testNVal2");
        tmp = example1.getAllProp1_N().toArray(new String[0]);
        Arrays.sort(tmp);
        Assert.assertEquals("testNVal1", tmp[0]);
        Assert.assertEquals("testNVal2", tmp[1]);
        example1.remProp1_N("testNVal1");
        tmp = example1.getAllProp1_N().toArray(new String[0]);
        Assert.assertEquals(1, tmp.length);
        Assert.assertEquals("testNVal2", tmp[0]);
        try {
            example1.remProp1_N("testNVal2");
            Assert.fail();
        } catch (RuntimeException ignored) {
        }
        //Ref tests
        Example example2 = domain.make(Example.class, "http://example.com/test2");
        Assert.assertNotSame(example1, example2);
        Assert.assertNull(example1.getPropRef0_1());
        try {
            example1.setPropRef0_1(example2);
            Assert.fail();
        } catch (RuntimeException ignored) {
        }
        example2.setProp1_1("someval1");
        example2.addProp1_N("fill");
        example2.setDateProp1_1(LocalDate.MIN);
        example1.setPropRef0_1(example2);
        Assert.assertSame(example1.getPropRef0_1(), example2);

        Example example3 = domain.make(Example.class, "http://example.com/test3");
        example3.setProp1_1("someval1");
        Example example4 = domain.make(Example.class, "http://example.com/test4");
        example4.setProp1_1("someval1");
        Assert.assertEquals(0, example1.getAllPropRef0_N().size());
        example3.addProp1_N("fill");
        example3.setDateProp1_1(LocalDate.MIN);
        example1.addPropRef0_N(example3);
        example4.addProp1_N("fill");
        example4.setDateProp1_1(LocalDate.MIN);
        example1.addPropRef0_N(example4);
        Example tmp2[] = example1.getAllPropRef0_N().toArray(new Example[0]);
        Assert.assertTrue((tmp2[0] == example3 && tmp2[1] == example4) || (tmp2[0] == example4 && tmp2[1] == example3));
        example1.remPropRef0_N(example3);
        tmp2 = example1.getAllPropRef0_N().toArray(new Example[0]);
        Assert.assertEquals(1, tmp2.length);
        Assert.assertSame(tmp2[0], example4);

        //Enum test
        Assert.assertNull(example1.getPropEnum0_1());
        example1.setPropEnum0_1(EnumExample.TEST1);
        Assert.assertSame(example1.getPropEnum0_1(), EnumExample.TEST1);

        Assert.assertEquals(0, example1.getAllPropEnum0_N().size());
        example1.addPropEnum0_N(EnumExample.TEST2);
        example1.addPropEnum0_N(EnumExample.TEST3);
        EnumExample tmp3[] = example1.getAllPropEnum0_N().toArray(new EnumExample[0]);
        Assert.assertTrue((tmp3[0] == EnumExample.TEST3 && tmp3[1] == EnumExample.TEST2) || (tmp3[0] == EnumExample.TEST2 && tmp3[1] == EnumExample.TEST3));
        example1.remPropEnum0_N(EnumExample.TEST2);
        tmp3 = example1.getAllPropEnum0_N().toArray(new EnumExample[0]);
        Assert.assertEquals(1, tmp3.length);
        Assert.assertSame(tmp3[0], EnumExample.TEST3);
        Assert.assertSame(tmp3[0].getParents()[0], EnumExample.TEST2);

        //Date test
        try {
            example1.getDateProp1_1();
            Assert.fail();
        } catch (RuntimeException ignored) {
        }
        example1.setDateProp1_1(LocalDate.parse("2009-01-09"));
        Assert.assertEquals(example1.getDateProp1_1(), LocalDate.parse("2009-01-09"));
        example1.setDateProp1_1(LocalDate.parse("2009-02-09"));
        example1.setDateProp1_1(LocalDate.parse("2009-03-09"));
        example1.setDateProp1_1(LocalDate.parse("2009-04-09"));
        example1.setDateProp1_1(LocalDate.parse("2009-05-09"));
        example1.setDateProp1_1(LocalDate.parse("2009-06-09"));
        Assert.assertEquals(example1.getDateProp1_1(), LocalDate.parse("2009-06-09"));

        example1.setDateTimeProp1_1(LocalDateTime.parse("2009-02-09T00:00:20.123"));
        Assert.assertEquals(example1.getDateTimeProp1_1(), LocalDateTime.parse("2009-02-09T00:00:20.123"));

        Assert.assertEquals(0, example1.getAllRefListProp0_1().size());
        Assert.assertNull(example1.getRefListProp0_1(0));
        example1.addRefListProp0_1(example2);
        example1.addRefListProp0_1(example3);
        Assert.assertEquals(2, example1.getAllRefListProp0_1().size());
        Assert.assertSame(example1.getRefListProp0_1(0), example2);
        Assert.assertSame(example1.getRefListProp0_1(1), example3);
        example1.setRefListProp0_1(null, 0);
        Assert.assertEquals(2, example1.getAllRefListProp0_1().size());
        example1.setRefListProp0_1(null, 1);
        Assert.assertEquals(0, example1.getAllRefListProp0_1().size());
        example1.setRefListProp0_1(example2, 4);
        Assert.assertSame(example1.getRefListProp0_1(4), example2);
        Assert.assertNull(example1.getRefListProp0_1(3));
        Assert.assertEquals(5, example1.getAllRefListProp0_1().size());
        Assert.assertSame(example1.getAllRefListProp0_1().get(4), example2);
        example1.addRefListProp0_1(example3);
        Assert.assertEquals(6, example1.getAllRefListProp0_1().size());
        Assert.assertSame(example1.getAllRefListProp0_1().get(5), example3);
        example1.addRefListProp0_1(example3);
        example1.addRefListProp0_1(example3);
        Assert.assertEquals(8, example1.getAllRefListProp0_1().size());
        example1.remRefListProp0_1(example3);
        Assert.assertEquals(5, example1.getAllRefListProp0_1().size());

        Assert.assertEquals(0, example1.getAllRefListListProp0_1().size());
        try {
            example1.getRefListListProp0_1(0);
            Assert.fail();
        } catch (ListIndexException ignored) {
        }
        try {
            example1.setRefListListProp0_1(example2, 4);
            Assert.fail();
        } catch (ListIndexException ignored) {
        }
        example1.addRefListListProp0_1(example2);
        Assert.assertSame(example1.getRefListListProp0_1(0), example2);
        example1.addRefListListProp0_1(example3);
        Assert.assertEquals(2, example1.getAllRefListListProp0_1().size());
        Assert.assertSame(example1.getRefListListProp0_1(0), example2);
        Assert.assertSame(example1.getRefListListProp0_1(1), example3);
        example1.remRefListListProp0_1(example2);
        Assert.assertEquals(1, example1.getAllRefListListProp0_1().size());
        example1.remRefListListProp0_1(example3);
        Assert.assertEquals(0, example1.getAllRefListListProp0_1().size());
        try {
            example1.setRefListListProp0_1(example2, 4);
            Assert.fail();
        } catch (ListIndexException ignored) {
        }
    }

    public void test2() throws Exception {
        //Test with anonymous nodes
        Domain domain = new Domain(new RDFSimpleCon(""));
        Example example1 = domain.make(Example.class, null);
        Assert.assertNotSame(example1, domain.make(Example.class, null));
        Assert.assertNull(example1.getProp0_1());
        example1.setProp0_1("test1");
        Assert.assertEquals("test1", example1.getProp0_1());
        try {
            example1.getProp1_1();
            Assert.fail();
        } catch (RuntimeException ignored) {
        }
        example1.setProp1_1("test2");
        Assert.assertEquals("test2", example1.getProp1_1());
        example1.setProp1_1("test3");
        example1.setProp1_1("test1");
        example1.setProp1_1("aaa");
        example1.setProp1_1("zzz");
        example1.setProp1_1("test2");
        Assert.assertEquals("test2", example1.getProp1_1());
        Assert.assertEquals("test1", example1.getProp0_1());
        Assert.assertEquals(0, example1.getAllProp0_N().size());
        example1.addProp0_N("testVal1");
        example1.addProp0_N("testVal2");
        String tmp[] = example1.getAllProp0_N().toArray(new String[0]);
        Arrays.sort(tmp);
        Assert.assertEquals(2, tmp.length);
        Assert.assertEquals("testVal1", tmp[0]);
        Assert.assertEquals("testVal2", tmp[1]);
        example1.remProp0_N("testVal1");
        tmp = example1.getAllProp0_N().toArray(new String[0]);
        Assert.assertEquals(1, tmp.length);
        Assert.assertEquals("testVal2", tmp[0]);
        try {
            example1.remProp0_N("testVal1");
            Assert.fail();
        } catch (RuntimeException ignored) {

        }
        example1.remProp0_N("testVal2");
        Assert.assertEquals(0, example1.getAllProp0_N().size());

        try {
            example1.getAllProp1_N();
            Assert.fail();
        } catch (RuntimeException ignored) {
        }
        example1.addProp1_N("testNVal1");
        example1.addProp1_N("testNVal2");
        tmp = example1.getAllProp1_N().toArray(new String[0]);
        Arrays.sort(tmp);
        Assert.assertEquals("testNVal1", tmp[0]);
        Assert.assertEquals("testNVal2", tmp[1]);
        example1.remProp1_N("testNVal1");
        tmp = example1.getAllProp1_N().toArray(new String[0]);
        Assert.assertEquals(1, tmp.length);
        Assert.assertEquals("testNVal2", tmp[0]);
        try {
            example1.remProp1_N("testNVal2");
            Assert.fail();
        } catch (RuntimeException ignored) {
        }
        //Ref tests
        Example example2 = domain.make(Example.class, null);
        Assert.assertNotSame(example1, example2);
        Assert.assertNull(example1.getPropRef0_1());
        try {
            example1.setPropRef0_1(example2);
            Assert.fail();
        } catch (RuntimeException ignored) {
        }
        example2.setProp1_1("someval1");
        example2.addProp1_N("fill");
        example2.setDateProp1_1(LocalDate.MIN);
        example1.setPropRef0_1(example2);
        Assert.assertSame(example1.getPropRef0_1(), example2);

        Example example3 = domain.make(Example.class, null);
        example3.setProp1_1("someval1");
        Example example4 = domain.make(Example.class, null);
        example4.setProp1_1("someval1");
        Assert.assertEquals(0, example1.getAllPropRef0_N().size());
        example3.addProp1_N("fill");
        example3.setDateProp1_1(LocalDate.MIN);
        example1.addPropRef0_N(example3);
        example4.addProp1_N("fill");
        example4.setDateProp1_1(LocalDate.MIN);
        example1.addPropRef0_N(example4);
        Example tmp2[] = example1.getAllPropRef0_N().toArray(new Example[0]);
        Assert.assertTrue((tmp2[0] == example3 && tmp2[1] == example4) || (tmp2[0] == example4 && tmp2[1] == example3));
        example1.remPropRef0_N(example3);
        tmp2 = example1.getAllPropRef0_N().toArray(new Example[0]);
        Assert.assertEquals(1, tmp2.length);
        Assert.assertSame(tmp2[0], example4);

        //Enum test
        Assert.assertNull(example1.getPropEnum0_1());
        example1.setPropEnum0_1(EnumExample.TEST1);
        Assert.assertSame(example1.getPropEnum0_1(), EnumExample.TEST1);

        Assert.assertEquals(0, example1.getAllPropEnum0_N().size());
        example1.addPropEnum0_N(EnumExample.TEST2);
        example1.addPropEnum0_N(EnumExample.TEST3);
        EnumExample tmp3[] = example1.getAllPropEnum0_N().toArray(new EnumExample[0]);
        Assert.assertTrue((tmp3[0] == EnumExample.TEST3 && tmp3[1] == EnumExample.TEST2) || (tmp3[0] == EnumExample.TEST2 && tmp3[1] == EnumExample.TEST3));
        example1.remPropEnum0_N(EnumExample.TEST2);
        tmp3 = example1.getAllPropEnum0_N().toArray(new EnumExample[0]);
        Assert.assertEquals(1, tmp3.length);
        Assert.assertSame(tmp3[0], EnumExample.TEST3);
        Assert.assertSame(tmp3[0].getParents()[0], EnumExample.TEST2);

        //Date test
        try {
            example1.getDateProp1_1();
            Assert.fail();
        } catch (RuntimeException ignored) {
        }
        example1.setDateProp1_1(LocalDate.parse("2009-01-09"));
        Assert.assertEquals(example1.getDateProp1_1(), LocalDate.parse("2009-01-09"));
        example1.setDateProp1_1(LocalDate.parse("2009-02-09"));
        example1.setDateProp1_1(LocalDate.parse("2009-03-09"));
        example1.setDateProp1_1(LocalDate.parse("2009-04-09"));
        example1.setDateProp1_1(LocalDate.parse("2009-05-09"));
        example1.setDateProp1_1(LocalDate.parse("2009-06-09"));
        Assert.assertEquals(example1.getDateProp1_1(), LocalDate.parse("2009-06-09"));

        Assert.assertEquals(0, example1.getAllRefListProp0_1().size());
        Assert.assertNull(example1.getRefListProp0_1(0));
        example1.addRefListProp0_1(example2);
        example1.addRefListProp0_1(example3);
        Assert.assertEquals(2, example1.getAllRefListProp0_1().size());
        Assert.assertSame(example1.getRefListProp0_1(0), example2);
        Assert.assertSame(example1.getRefListProp0_1(1), example3);
        example1.setRefListProp0_1(null, 0);
        Assert.assertEquals(2, example1.getAllRefListProp0_1().size());
        example1.setRefListProp0_1(null, 1);
        Assert.assertEquals(0, example1.getAllRefListProp0_1().size());
        example1.setRefListProp0_1(example2, 4);
        Assert.assertSame(example1.getRefListProp0_1(4), example2);
        Assert.assertNull(example1.getRefListProp0_1(3));
        Assert.assertEquals(5, example1.getAllRefListProp0_1().size());
        Assert.assertSame(example1.getAllRefListProp0_1().get(4), example2);
        example1.addRefListProp0_1(example3);
        Assert.assertEquals(6, example1.getAllRefListProp0_1().size());
        Assert.assertSame(example1.getAllRefListProp0_1().get(5), example3);
        example1.addRefListProp0_1(example3);
        example1.addRefListProp0_1(example3);
        Assert.assertEquals(8, example1.getAllRefListProp0_1().size());
        example1.remRefListProp0_1(example3);
        Assert.assertEquals(5, example1.getAllRefListProp0_1().size());

        Assert.assertEquals(0, example1.getAllRefListListProp0_1().size());
        try {
            example1.getRefListListProp0_1(0);
            Assert.fail();
        } catch (ListIndexException ignored) {
        }
        try {
            example1.setRefListListProp0_1(example2, 4);
            Assert.fail();
        } catch (ListIndexException ignored) {
        }
        example1.addRefListListProp0_1(example2);
        Assert.assertSame(example1.getRefListListProp0_1(0), example2);
        example1.addRefListListProp0_1(example3);
        Assert.assertEquals(2, example1.getAllRefListListProp0_1().size());
        Assert.assertSame(example1.getRefListListProp0_1(0), example2);
        Assert.assertSame(example1.getRefListListProp0_1(1), example3);
        example1.remRefListListProp0_1(example2);
        Assert.assertEquals(1, example1.getAllRefListListProp0_1().size());
        example1.remRefListListProp0_1(example3);
        Assert.assertEquals(0, example1.getAllRefListListProp0_1().size());
        try {
            example1.setRefListListProp0_1(example2, 4);
            Assert.fail();
        } catch (ListIndexException ignored) {
        }
    }


    public void testGetSubclass() throws Exception {
        Domain domain = new Domain(new RDFSimpleCon(""));
        Assert.assertSame(domain.getRDFSimpleCon().createResource("http://test.com/1"), domain.getRDFSimpleCon().createResource("http://test.com/1"));
        Example root = domain.make(Example.class, "http://example.com/test/root");
        TestA testA = domain.make(TestA.class, "http://example.com/test/testA");
        testA.setPropA("test");
        testA.setProp1_1("fill");
        testA.addProp1_N("fill");
        testA.setDateProp1_1(LocalDate.MIN);
        root.setPropRef0_1(testA);
        domain.save("./testtemp.ttl");
        domain.close();
        domain = new Domain(new RDFSimpleCon("file://./testtemp.ttl"));
//    domain.getRDFSimpleCon().getObjects("http://example.com/test/testA",)
        Example testAReadAgain = domain.make(Example.class, "http://example.com/test/testA");
        Assert.assertEquals("http://empusa.org/test#TestA", testAReadAgain.getClassTypeIri());
        Assert.assertSame(testAReadAgain.getClass(), TestAImpl.class);
    }

    public void testTemp() throws Exception {
        if (new File("../../GBOLOntology/root-ontology.ttl").exists() && new File("../../GBOLOntology/GbolAdditional.ttl").exists()) {
            String[] args = new String[]{"-i", "../../GBOLOntology/GbolAdditional.ttl", "../../GBOLOntology/root-ontology.ttl",
                    "-o", "../../GBOLApi", "-r", "../../RGBOLApi",
                    "-owl", "../../GBOLOntology/generated/GBOL.owl",
                    "-ShExC", "../../GBOLOntology/generated/GBOL.shexc",
                    "-ShExR", "../../GBOLOntology/generated/GBOL.shexr",
                    "-rg", "../../GBOLOntology/generated/GBOL_RDF2Graph.ttl",
                    "-doc", "../../gbol.gitlab.io",//"-skipNarrowingProperties"
                    "-jsonld", "../../GBOLOntology/generated/GBOL_JSONLDFrame.json"};
            org.empusa.codegen.Main.main(args);
        }
    }
}
