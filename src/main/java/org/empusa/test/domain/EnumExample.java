package org.empusa.test.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://empusa.org/test# ontology
 */
public enum EnumExample implements EnumClass {
  TEST1("http://empusa.org/test#TEST1",new EnumExample[]{}),

  TEST2("http://empusa.org/test#TEST2",new EnumExample[]{TEST1}),

  TEST3("http://empusa.org/test#TEST3",new EnumExample[]{TEST2}),

  TEST4("http://empusa.org/test#TEST4",new EnumExample[]{TEST3});

  private EnumExample[] parents;

  private String iri;

  private EnumExample(String iri, EnumExample[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static EnumExample make(String iri) {
    for(EnumExample item : EnumExample.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
