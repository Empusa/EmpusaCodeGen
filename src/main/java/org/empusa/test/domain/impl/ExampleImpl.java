package org.empusa.test.domain.impl;

import java.lang.String;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.empusa.test.domain.EnumExample;
import org.empusa.test.domain.Example;

/**
 * Code generated from http://empusa.org/test# ontology
 */
public class ExampleImpl extends OWLThingImpl implements Example {
  public static final String TypeIRI = "http://empusa.org/test#Example";

  protected ExampleImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Example make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ExampleImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Example.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Example.class,false);
          if(toRet == null) {
            toRet = new ExampleImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Example)) {
          throw new RuntimeException("Instance of org.empusa.test.domain.impl.ExampleImpl expected");
        }
      }
      return (Example)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://empusa.org/test#prop1_1");
    this.checkCardMin1("http://empusa.org/test#prop1_N");
    this.checkCardMin1("http://empusa.org/test#dateProp1_1");
  }

  public String getProp0_1() {
    return this.getStringLit("http://empusa.org/test#prop0_1",true);
  }

  public void setProp0_1(String val) {
    this.setStringLit("http://empusa.org/test#prop0_1",val);
  }

  public String getProp1_1() {
    return this.getStringLit("http://empusa.org/test#prop1_1",false);
  }

  public void setProp1_1(String val) {
    this.setStringLit("http://empusa.org/test#prop1_1",val);
  }

  public LocalDateTime getDateTimeProp1_1() {
    return this.getDateTimeLit("http://empusa.org/test#dateTimeProp1_1",true);
  }

  public void setDateTimeProp1_1(LocalDateTime val) {
    this.setDateTimeLit("http://empusa.org/test#dateTimeProp1_1",val);
  }

  public void remProp1_N(String val) {
    this.remStringLit("http://empusa.org/test#prop1_N",val,false);
  }

  public List<? extends String> getAllProp1_N() {
    return this.getStringLitSet("http://empusa.org/test#prop1_N",false);
  }

  public void addProp1_N(String val) {
    this.addStringLit("http://empusa.org/test#prop1_N",val);
  }

  public EnumExample getPropEnum0_1() {
    return this.getEnum("http://empusa.org/test#propEnum0_1",true,EnumExample.class);
  }

  public void setPropEnum0_1(EnumExample val) {
    this.setEnum("http://empusa.org/test#propEnum0_1",val,EnumExample.class);
  }

  public Example getPropRef0_1() {
    return this.getRef("http://empusa.org/test#propRef0_1",true,Example.class);
  }

  public void setPropRef0_1(Example val) {
    this.setRef("http://empusa.org/test#propRef0_1",val,Example.class);
  }

  public void remPropRef0_N(Example val) {
    this.remRef("http://empusa.org/test#propRef0_N",val,true);
  }

  public List<? extends Example> getAllPropRef0_N() {
    return this.getRefSet("http://empusa.org/test#propRef0_N",true,Example.class);
  }

  public void addPropRef0_N(Example val) {
    this.addRef("http://empusa.org/test#propRef0_N",val);
  }

  public void remPropEnum0_N(EnumExample val) {
    this.remEnum("http://empusa.org/test#propEnum0_N",val,true);
  }

  public List<? extends EnumExample> getAllPropEnum0_N() {
    return this.getEnumSet("http://empusa.org/test#propEnum0_N",true,EnumExample.class);
  }

  public void addPropEnum0_N(EnumExample val) {
    this.addEnum("http://empusa.org/test#propEnum0_N",val);
  }

  public LocalDate getDateProp1_1() {
    return this.getDateLit("http://empusa.org/test#dateProp1_1",false);
  }

  public void setDateProp1_1(LocalDate val) {
    this.setDateLit("http://empusa.org/test#dateProp1_1",val);
  }

  public void remProp0_N(String val) {
    this.remStringLit("http://empusa.org/test#prop0_N",val,true);
  }

  public List<? extends String> getAllProp0_N() {
    return this.getStringLitSet("http://empusa.org/test#prop0_N",true);
  }

  public void addProp0_N(String val) {
    this.addStringLit("http://empusa.org/test#prop0_N",val);
  }

  public Example getRefListProp0_1(int index) {
    return this.getRefListAtIndex("http://empusa.org/test#refListProp0_1",true,Example.class,index);
  }

  public List<? extends Example> getAllRefListProp0_1() {
    return this.getRefList("http://empusa.org/test#refListProp0_1",true,Example.class);
  }

  public void addRefListProp0_1(Example val) {
    this.addRefList("http://empusa.org/test#refListProp0_1",val);
  }

  public void setRefListProp0_1(Example val, int index) {
    this.setRefList("http://empusa.org/test#refListProp0_1",val,true,index);
  }

  public void remRefListProp0_1(Example val) {
    this.remRefList("http://empusa.org/test#refListProp0_1",val,true);
  }

  public Example getRefListListProp0_1(int index) {
    return this.getRefListAtIndexList("http://empusa.org/test#refListListProp0_1",true,Example.class,index);
  }

  public List<? extends Example> getAllRefListListProp0_1() {
    return this.getRefListList("http://empusa.org/test#refListListProp0_1",true,Example.class);
  }

  public void addRefListListProp0_1(Example val) {
    this.addRefListList("http://empusa.org/test#refListListProp0_1",val);
  }

  public void setRefListListProp0_1(Example val, int index) {
    this.setRefListList("http://empusa.org/test#refListListProp0_1",val,true,index);
  }

  public void remRefListListProp0_1(Example val) {
    this.remRefListList("http://empusa.org/test#refListListProp0_1",val,true);
  }
}
