package org.empusa.test.domain;

import java.lang.String;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://empusa.org/test# ontology
 */
public interface Example extends OWLThing {
  String getProp0_1();

  void setProp0_1(String val);

  String getProp1_1();

  void setProp1_1(String val);

  LocalDateTime getDateTimeProp1_1();

  void setDateTimeProp1_1(LocalDateTime val);

  void remProp1_N(String val);

  List<? extends String> getAllProp1_N();

  void addProp1_N(String val);

  EnumExample getPropEnum0_1();

  void setPropEnum0_1(EnumExample val);

  Example getPropRef0_1();

  void setPropRef0_1(Example val);

  void remPropRef0_N(Example val);

  List<? extends Example> getAllPropRef0_N();

  void addPropRef0_N(Example val);

  void remPropEnum0_N(EnumExample val);

  List<? extends EnumExample> getAllPropEnum0_N();

  void addPropEnum0_N(EnumExample val);

  LocalDate getDateProp1_1();

  void setDateProp1_1(LocalDate val);

  void remProp0_N(String val);

  List<? extends String> getAllProp0_N();

  void addProp0_N(String val);

  Example getRefListProp0_1(int index);

  List<? extends Example> getAllRefListProp0_1();

  void addRefListProp0_1(Example val);

  void setRefListProp0_1(Example val, int index);

  void remRefListProp0_1(Example val);

  Example getRefListListProp0_1(int index);

  List<? extends Example> getAllRefListListProp0_1();

  void addRefListListProp0_1(Example val);

  void setRefListListProp0_1(Example val, int index);

  void remRefListListProp0_1(Example val);
}
