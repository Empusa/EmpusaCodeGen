package org.empusa.test.domain;

import java.lang.String;

/**
 * Code generated from http://empusa.org/test# ontology
 */
public interface TestB extends Example {
  String getPropB();

  void setPropB(String val);
}
