package org.empusa.test.domain;

import java.lang.String;

/**
 * Code generated from http://empusa.org/test# ontology
 */
public interface TestA extends Example {
  String getPropA();

  void setPropA(String val);
}
