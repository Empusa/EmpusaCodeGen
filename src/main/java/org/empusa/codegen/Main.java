package org.empusa.codegen;

import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import org.empusa.codegen.gen.*;
import org.empusa.codegen.gen.owl.OwlGen;

public class Main
{
  RDFSimpleCon con;
  private CommandOptions args;

  public static void main(String args[]) throws Exception
  {
    new Main(args);
  }
   
  public Main(String args[]) throws Exception
  {
    this.args = new CommandOptions(args);
    con = new RDFSimpleCon("file://" + this.args.inOwlFile.get(0));
    for(int i = 1;i < this.args.inOwlFile.size();i++)
    {
      RDFSimpleCon temp = new RDFSimpleCon("file://" + this.args.inOwlFile.get(i));
      con.addAll(temp);
      temp.close();
    }
    OntologySet ontology = new OntologySet(con);

    if (this.args.outProjectDir != null) {
      JavaCodeGen codeGen = new JavaCodeGen(ontology, this.args);
      codeGen.genCode();
    }
    
    if(this.args.RDF2GraphOutFile != null)
    {
      RDF2GraphGen rdf2GraphGen = new RDF2GraphGen(ontology,this.args);
      rdf2GraphGen.gen();
    }
    
    if(this.args.outRProjectDir != null)
    {
      System.out.println("Generating R code");
      RCodeGen rCodeGen = new RCodeGen(ontology,this.args);
      rCodeGen.genCode();
    }
    
    if(this.args.shexROutFile != null || this.args.shexCOutFile != null)
    {
      if(this.args.shexROutFile != null)
        System.out.println("Generating ShexR file");
      ShexGen shexGen = new ShexGen(ontology,this.args);
      shexGen.gen();
      if(this.args.shexCOutFile != null)
        System.out.println("Generating ShexC file");
    }
    
    if(this.args.docDir != null)
    {
      System.out.println("Generating documentation site for mkdocs");
      DocGen docgen = new DocGen(ontology,this.args);
      docgen.gen();
      System.out.println("Generated mkdocs_temp.yml, please copy into a complete mkdocs.yml file");
    }
    
    if(this.args.owlOutFile != null)
    {
      System.out.println("Generating official owl file");
      OwlGen owlgen = new OwlGen(ontology,this.args);
      owlgen.gen();
    }
    
    if(this.args.jsonFrameOutFile != null)
    {
      System.out.println("Generating json framing file");
      JSONLDFrameGen framegen = new JSONLDFrameGen(ontology,this.args);
      framegen.gen();
    }


    con.close();
  }
}
