package org.empusa.codegen;

import com.squareup.javapoet.ClassName;
import org.apache.commons.lang3.text.WordUtils;
import org.empusa.RDF2Graph.domain.Multiplicity;
import org.empusa.codegen.type.ExternalRefType;

import java.util.HashSet;

public class Field
{
  boolean isBag = false;
  boolean isSeq = false;
  boolean isList = false;
  boolean isOptional = false;
  boolean isArray = false;
  Multiplicity reverseMultipicity = Multiplicity.Zero_or_many;
  String name;
  String predIRI;
  private Type type;
  private Clazz owner;
  String comment;
  Property property;
  HashSet<Clazz> exludeTypeInFraming = new HashSet<Clazz>();

  private Field(Type type,Clazz owner)
  {
    this.type = type;
    this.owner = owner;
  }
  
  public static Field parse(OntologySet ontology,String line,String defNameSpace,Clazz parent,String comment) throws Exception
  {
    line = line.trim();
    String tmp[] = line.split(" ");
    if(tmp.length != 2)
      throw new Exception("Parse exception property: " + line + " " + defNameSpace + " " + parent.classIRI);
    String name = tmp[0];
    String def = tmp[1];
    if(name.startsWith(":"))
      name = name.substring(1);
    String predIri = defNameSpace + name;
    tmp = name.split(":");
    if(tmp.length == 2)
    {
      String iri = ontology.getPrefix(tmp[0]);
      if(iri == null)
        throw new Exception("Prefix unknown:" + tmp[0]);
      name = tmp[1];
      predIri = iri + name;      
    }
    if(!name.matches("[a-zA-Z_][0-9a-zA-Z_]*"))
      throw new Exception("Illegal name: " + line);
    
    Multiplicity reverseMultipicity = Multiplicity.none;
    boolean isBag = false;
    boolean isList = false;
    boolean isSeq = false;
    boolean isOptional = false;
    boolean isArray = false;
    if(def.endsWith("="))
    {
      isSeq = true;
      def = def.substring(0,def.length() - 1);
    }
    else if(def.endsWith("-"))
    {
      isBag = true;
      def = def.substring(0,def.length() - 1);
    }
    else if(def.endsWith("~"))
    {
      isList = true;
      def = def.substring(0,def.length() - 1);
    }
    if(def.endsWith("-+"))
    {
      reverseMultipicity = Multiplicity.One_or_many;
      def = def.substring(0,def.length() - 2);
    }
    else if(def.endsWith("-*"))
    {
      reverseMultipicity = Multiplicity.Zero_or_many;
      def = def.substring(0,def.length() - 2);
    }
    else if(def.endsWith("-?"))
    {
      reverseMultipicity = Multiplicity.Zero_or_one;
      def = def.substring(0,def.length() - 2);
    }
    else if(def.endsWith("-1"))
    {
      reverseMultipicity = Multiplicity.Exactly_one;
      def = def.substring(0,def.length() - 2);
    }
    if(def.endsWith("+"))
    {
      isArray = true;
      def = def.substring(0,def.length() - 1);
    }
    else if(def.endsWith("?"))
    {
      isOptional = true;
      def = def.substring(0,def.length() - 1);
    }
    else if(def.endsWith("*"))
    {
      isArray = true;
      isOptional = true;
      def = def.substring(0,def.length() - 1);
    }
    if(isBag && !isArray )
      throw new Exception("Bag must have multiplicity of 0:N or 1:N | " + line);
     
    Field field = null;
    if(def.startsWith("xsd:"))
    {
      if(reverseMultipicity != Multiplicity.none)
        throw new Exception("Reverse multiplicity only allowed for class instances");
      String xsdType = def.substring("xsd:".length());
      try
      {
        Class clazz = Class.forName("org.empusa.codegen.type." + WordUtils.capitalize(xsdType) + "Type");
        field = new Field((Type)clazz.getField("instance").get(null),parent);
      }
      catch(ClassNotFoundException e)
      {
        throw new Exception("xsd type unknown: " + xsdType);
      }
    }
    else if(def.startsWith("type:"))
    {
      if(reverseMultipicity != Multiplicity.none)
        throw new Exception("Reverse multiplicity only allowed for class instances");
      String enumType = def.substring("type:".length());
      
      Type clazz = ontology.getClass(ontology.getCon().expand(enumType));
      if(clazz == null)
        throw new Exception("Unknown class: " + def + " - " + line + " in " + parent.classIRI);
      field = new Field(clazz,parent);
    }
    else if(def.equals("IRI"))
    {
      if(reverseMultipicity != Multiplicity.none)
        throw new Exception("Reverse multiplity only allowed for class intances");
      field = new Field(ExternalRefType.instance,parent);
    }
    else if(def.startsWith("@"))
    {
      Type clazz = ontology.getClass(ontology.getCon().expand(def.substring(1)));
      if(clazz == null)
        throw new Exception("Unknown class: " + def + " - " + line + " | " + ontology.getCon().expand(def.substring(1)));
      field = new Field(clazz,parent);  
    }
    else
    {
      throw new Exception("Unknown type def: " + def + " for field " + name + " in class " + parent.getClassIRI());
    }
    
    field.name = name;
    field.predIRI = predIri;
    field.isBag = isBag;
    field.isSeq = isSeq;
    field.isList = isList;
    field.isArray = isArray;
    field.isOptional = isOptional;   
    field.reverseMultipicity = reverseMultipicity;
    field.comment = comment.trim();

    return field;
  }

  public void copyCommentFromParent(Clazz clazz) throws Exception
  {
    if(this.comment.equals("-"))
    {
      if(!copyCommentFromParent2(clazz))
        throw new Exception("Comment from parent field can not be found: " + clazz.getName() + " - " + this.name);
    }
  }
  
  public boolean copyCommentFromParent2(Clazz clazz)
  {
    Field parentField = clazz.getFieldByIri(this.predIRI);
    if(parentField != null && !parentField.comment.equals("-"))
    {
      this.comment = parentField.comment;
      return true;
    }
    for(Clazz parent : clazz.getParents())
    {
      if(copyCommentFromParent2(parent))
        return true;      
    }
    return false;
  }
  
  public boolean isBag()
  {
    return isBag;
  }

  public boolean isSeq()
  {
    return isSeq;
  }
  
  public boolean isList()
  {
    return isList;
  }

  public boolean isOptional()
  {
    return isOptional;
  }

  public boolean isArray()
  {
    return isArray;
  }

  public String getName()
  {
    return name;
  }

  public String getPredIRI()
  {
    return predIRI;
  }
  
  public Type getType()
  {
    return this.type;
  }
  
  public ClassName getClassType()
  {
    return type.getClassType();
  }

  public String getClassTypeName()
  {
    return type.getClassTypeName();
  }
  
  public boolean needExpectedType()
  {
    return type.needExpectedType();
  }

  public Multiplicity getReverseMultipicity()
  {
    return reverseMultipicity;
  }
  
  public String getComment()
  {
    return comment;
  }

  public Clazz getOwner()
  {
    return owner;
  }

  public Property getProperty()
  {
    return property;
  }   
  
  public void addExcludeTypeInFraming(Clazz clazz)
  {
    this.exludeTypeInFraming.add(clazz);
  }

  public HashSet<Clazz> getExludeTypeInFraming()
  {
    return exludeTypeInFraming;
  }  
}
