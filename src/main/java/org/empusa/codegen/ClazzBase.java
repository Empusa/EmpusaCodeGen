package org.empusa.codegen;

public abstract class ClazzBase extends Type
{
  protected Ontology ontology;
  protected String classIRI;
  public ClazzBase(Ontology ontology,String classIRI)
  {
    this.ontology = ontology;
    this.classIRI = classIRI;
  }
  
  public abstract void preParse() throws Exception;  
  public abstract void postParse() throws Exception;
  public abstract void accept(OntologyVisitor visitor)  throws Exception;

  public OntologySet getOntologySet()
  {
    return ontology.getOntologySet();
  }  

  public Ontology getOntology()
  {
    return ontology;
  }

  public String getClassIRI()
  {
    return classIRI;
  }   
  
  public String toString()
  {
    return this.getClassIRI();
  }
}
