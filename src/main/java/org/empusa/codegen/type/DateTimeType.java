package org.empusa.codegen.type;

import org.empusa.codegen.Type;

import com.squareup.javapoet.ClassName;

public class DateTimeType extends XSDDataType
{
  public static final DateTimeType instance = new DateTimeType();
  @Override
  public ClassName getClassType()
  {
    return ClassName.get("java.time","LocalDateTime");
  }

  @Override
  public String getClassTypeName()
  {
    return "DateTimeLit";
  }

  @Override
  public boolean needExpectedType()
  {
    return false;
  }

  @Override
  public String getName()
  {
    return "DateTime";
  }
  
  @Override
  public String getClassXSDType()
  {
    return "xsd:dateTime";
  }
  
  @Override
  public boolean matches(Type other)
  {
    return other instanceof DateTimeType;
  }
}
