package org.empusa.codegen.type;

import org.empusa.codegen.Type;

import com.squareup.javapoet.ClassName;

public class DateType extends XSDDataType
{
  public static final DateType instance = new DateType();
  @Override
  public ClassName getClassType()
  {
    return ClassName.get("java.time","LocalDate");
  }

  @Override
  public String getClassTypeName()
  {
    return "DateLit";
  }

  @Override
  public boolean needExpectedType()
  {
    return false;
  }

  @Override
  public String getName()
  {
    return "Date";
  }
  @Override
  public String getClassXSDType()
  {
    // Be aware xsd:date does not exists... according to hermit
    return "xsd:dateTime";
  }
  
  @Override
  public boolean matches(Type other)
  {
    return other instanceof DateType;
  }
}
