package org.empusa.codegen.type;

import com.squareup.javapoet.ClassName;
import org.empusa.codegen.Type;

public class DoubleType extends XSDDataType
{
  public static final DoubleType instance = new DoubleType();
  @Override
  public ClassName getClassType()
  {
    return ClassName.get("java.lang","Double");
  }

  @Override
  public String getClassTypeName()
  {
    return "DoubleLit";
  } 
  
  @Override
  public boolean needExpectedType()
  {
    return false;
  }

  @Override
  public String getName()
  {
    return "Double";
  }
  @Override
  public String getClassXSDType()
  {
    return "xsd:double";
  }
  
  @Override
  public boolean matches(Type other)
  {
    return other instanceof DoubleType;
  }
}
