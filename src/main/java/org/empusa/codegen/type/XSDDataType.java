package org.empusa.codegen.type;

import org.empusa.codegen.Type;

public abstract class XSDDataType extends Type
{
  public abstract String getClassXSDType();
}
