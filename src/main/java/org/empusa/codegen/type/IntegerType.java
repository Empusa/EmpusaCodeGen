package org.empusa.codegen.type;

import com.squareup.javapoet.ClassName;
import org.empusa.codegen.Type;

public class IntegerType extends XSDDataType
{
  public static final IntegerType instance = new IntegerType();
  @Override
  public ClassName getClassType()
  {
    return ClassName.get("java.lang","Integer");
  }

  @Override
  public String getClassTypeName()
  {
    return "IntegerLit";
  } 
  
  @Override
  public boolean needExpectedType()
  {
    return false;
  }

  @Override
  public String getName()
  {
    return "Integer";
  }
  
  @Override
  public String getClassXSDType()
  {
    return "xsd:integer";
  }
  
  @Override
  public boolean matches(Type other)
  {
    return other instanceof IntegerType;
  }
}
