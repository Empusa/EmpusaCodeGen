package org.empusa.codegen.type;

import com.squareup.javapoet.ClassName;
import org.empusa.codegen.Type;

public class BooleanType extends XSDDataType
{
  public static final BooleanType instance = new BooleanType();
  @Override
  public ClassName getClassType()
  {
    return ClassName.get("java.lang","Boolean");
  }

  @Override 
  public String getClassTypeName()
  {
    return "BooleanLit";
  } 
  
  @Override
  public boolean needExpectedType()
  {
    return false;
  }

  @Override
  public String getName()
  {
    return "Boolean";
  }

  @Override
  public String getClassXSDType()
  {
    return "xsd:boolean";
  }

  @Override
  public boolean matches(Type other)
  {
    return other instanceof BooleanType;
  }
}
