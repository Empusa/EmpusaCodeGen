package org.empusa.codegen.type;

import org.empusa.codegen.Type;

import com.squareup.javapoet.ClassName;

public class ExternalRefType extends XSDDataType
{
  public static final ExternalRefType instance = new ExternalRefType();
  @Override
  public ClassName getClassType()
  {
    return ClassName.get("java.lang","String");
  }

  @Override
  public String getClassTypeName()
  {
    return "ExternalRef";
  }
  
  @Override
  public boolean needExpectedType()
  {
    return false;
  }

  @Override
  public String getName()
  {
    return "IRI";
  }
  
  @Override
  public String getClassXSDType()
  {
    return "xsd:anyURI";
  }
  
  @Override
  public boolean matches(Type other)
  {
    return other instanceof ExternalRefType;
  }
}
