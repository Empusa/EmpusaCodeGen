package org.empusa.codegen.type;

import org.empusa.codegen.Type;

import com.squareup.javapoet.ClassName;

public class StringType extends XSDDataType
{
  public static final StringType instance = new StringType();
  public StringType()
  {

  }

  @Override
  public ClassName getClassType()
  {
    return ClassName.get("java.lang","String");
  }

  @Override
  public String getClassTypeName()
  {
    return "StringLit";
  }
  
  @Override 
  public boolean needExpectedType()
  {
    return false;
  }

  @Override
  public String getName()
  {
    return "string";
  }
  
  @Override
  public String getClassXSDType()
  {
    return "xsd:string";
  }
  
  @Override
  public boolean matches(Type other)
  {
    return other instanceof StringType;
  }
  
  
}
