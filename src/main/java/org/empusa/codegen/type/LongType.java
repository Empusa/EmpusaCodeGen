package org.empusa.codegen.type;

import org.empusa.codegen.Field;
import org.empusa.codegen.Type;
import org.empusa.codegen.gen.JavaCodeGen;

import com.squareup.javapoet.ClassName;

public class LongType extends XSDDataType
{
  public static final LongType instance = new LongType();
  @Override
  public ClassName getClassType()
  {
    return ClassName.get("java.lang","Long");
  }

  @Override
  public String getClassTypeName()
  {
    return "LongLit";
  } 
  
  @Override
  public boolean needExpectedType()
  {
    return false;
  }

  @Override
  public String getName()
  {
    return "Long";
  }
  @Override
  public String getClassXSDType()
  {
    return "xsd:long";
  }
  
  @Override
  public boolean matches(Type other)
  {
    return other instanceof LongType;
  }
}
