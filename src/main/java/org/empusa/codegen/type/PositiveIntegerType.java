package org.empusa.codegen.type;

public class PositiveIntegerType extends IntegerType
{
  public static final PositiveIntegerType instance = new PositiveIntegerType();
}
