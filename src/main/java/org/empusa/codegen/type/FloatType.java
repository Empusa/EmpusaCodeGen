package org.empusa.codegen.type;

import com.squareup.javapoet.ClassName;
import org.empusa.codegen.Type;

public class FloatType extends XSDDataType
{
  public static final FloatType instance = new FloatType();
  @Override
  public ClassName getClassType()
  {
    return ClassName.get("java.lang","Float");
  }

  @Override
  public String getClassTypeName()
  {
    return "FloatLit";
  } 
  
  @Override
  public boolean needExpectedType()
  {
    return false;
  }

  @Override
  public String getName()
  {
    return "Float";
  }
  
  @Override
  public String getClassXSDType()
  {
    return "xsd:float";
  }
  
  @Override
  public boolean matches(Type other)
  {
    return other instanceof FloatType;
  }
}
