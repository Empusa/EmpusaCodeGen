package org.empusa.codegen;

import java.util.HashSet;
import java.util.LinkedList;

import nl.wur.ssb.RDFSimpleCon.ResultLine;

public class Property
{
  private String iri;
  private int propertyType; //0 = unknown, 1 = object property, 2 = datatype property
  private LinkedList<Field> fields = new LinkedList<Field>();
  private HashSet<Clazz> domain = new HashSet<Clazz>();
  private HashSet<Type> range = new HashSet<Type>();
  private LinkedList<Property> parents = new LinkedList<Property>();
  private LinkedList<Property> childs = new LinkedList<Property>();
  
  public Property(String iri)
  {
    this.iri = iri;
  }
  
  public void addField(Field field)
  {
    field.property = this;
    this.fields.add(field);
    this.domain.add(field.getOwner());
    this.range.add(field.getType());
  }
  
  public HashSet<Clazz> getDomain()
  {
    return this.domain;
  }
  
  public HashSet<Type> getRange()
  {
    return this.range;
  }
  
  public String getPredIri()
  {
    return this.iri;
  }
  
  public void loadParentProps(OntologySet ontologySet) throws Exception
  {
    for(ResultLine line : ontologySet.getCon().runQuery("getParentProps.sparql",false,this.iri))
    {
      Property prop = ontologySet.getProperties().get(line.getIRI("parentProp"));
      
      if(prop == null)
      {
        System.out.println("parent prop not used: " + line.getIRI("parentProp"));
        prop = new Property(line.getIRI("parentProp"));
        ontologySet.addProperty(prop);
        prop.loadParentProps(ontologySet);
      }
      addParent(prop);
    }
  }
  
  public void determineType(boolean recursion)
  {
    if(propertyType != 0)
      return;//already determined or already being determined
    propertyType = -1;
    if(this.range.size() > 1)
      propertyType = range.iterator().next() instanceof ClazzBase ? 1 : 2;
    for(Property child : this.childs)
    {
      child.determineType(true);
      if(child.propertyType > 0)
      {
        this.propertyType = child.propertyType;
        return;
      }
    }
    for(Property parent : this.parents)
    {
      parent.determineType(true);
      if(parent.propertyType > 0)
      {
        this.propertyType = parent.propertyType;
        return;
      }
    }
    if(recursion)
      this.propertyType = 0; //failed to determine it
    else 
      this.propertyType = 1;// default to object property
  }
  
  public void addParent(Property prop)
  {
    this.parents.add(prop);
    prop.childs.add(this);
  }

  public LinkedList<Property> getParents()
  {
    return parents;
  }

  public LinkedList<Property> getChilds()
  {
    return childs;
  }

  public LinkedList<Field> getFields()
  {
    return fields;
  }  
  
  public boolean isObjectProperty()
  {
    return this.propertyType != 2;
  }
}
