package org.empusa.codegen;

import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

public class OntologySet
{
  private HashMap<String,ClazzBase> classes = new HashMap<>();
  private HashMap<String,Property> properties = new HashMap<>();
  private HashMap<String,Ontology> ontologies = new HashMap<>();
  private RDFSimpleCon con;
  //private String baseIri;
  private String codeProjectGroup;
  private String codeProjectName;
  private String codeProjectVersion;
  private String codeProjectFullName;
  private String codeProjectDescription;  
  private LinkedList<Clazz> frameRoot = new LinkedList<>();
  
  public OntologySet(RDFSimpleCon con) throws Exception
  {
    this.con = con;
    ResultLine ontResult = con.runQuery("getOntologyBase.sparql",false).iterator().next();
    String baseIri = ontResult.getIRI("ontology");
    if (baseIri == null) {
      baseIri = "org.empusa.api";
    }
    if(codeProjectName == null) {
      codeProjectName = baseIri.replaceAll("\\.", "").replaceAll("http.?:\\/\\/", "").replaceAll("/",".") + "API";
    }
    codeProjectGroup =  ontResult.getLitString("codeProjectGroup");
    if(codeProjectGroup == null)
      codeProjectGroup = codeProjectName;
    codeProjectVersion = ontResult.getLitString("codeProjectVersion");
    if(codeProjectVersion == null)
      codeProjectVersion = "0.0.1";
    codeProjectFullName = ontResult.getLitString("codeProjectFullName");
    if(codeProjectFullName == null)
      codeProjectFullName = codeProjectName;
    codeProjectDescription = ontResult.getLitString("codeProjectDescription");
    if(codeProjectDescription == null)
      codeProjectDescription = "API for " + codeProjectFullName;
    
    for(ResultLine item : con.runQuery("getAllClasses.sparql",false))
    {
      loadClass(item.getIRI("class"));
    }
    for(ResultLine item : con.runQuery("getAllEnumTypes.sparql",false))
    {
      loadEnumClass(item.getIRI("class"));
    }
    for(ClazzBase clazz : this.classes.values())
    {
      clazz.preParse();
    }
    for(ClazzBase clazz : this.classes.values())
    {
      clazz.postParse();
    }
    for(Property prop : this.properties.values().toArray(new Property[0]))
    {
      prop.loadParentProps(this);
    }
    for(Property prop : this.properties.values().toArray(new Property[0]))
    {
      prop.determineType(false);
    }
    checkDependencyTree();
  }
  
  private void checkDependencyTree()
  {
    for(Ontology item : this.ontologies.values())
    {
      if(!item.checkDependTree(new HashSet<>()))
      {
        // throw new RuntimeException("Dependency tree loops for ontology: " + item.getBaseIri() + "\n" + item.getTree(new HashSet<Ontology>(),""));
        System.err.println("Dependency tree loops for ontology: " + item.getBaseIri() + "\n" + item.getTree(new HashSet<>(),""));
      }
    }    
  }  
  
  private void loadClass(String classIRI) throws Exception
  {
    if(classes.containsKey(classIRI))
      throw new Exception("class already created");
    if(classIRI.equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#Bag") || classIRI.equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#Seq"))
      return;
    new Clazz(this.getOntology(classIRI),classIRI);
  }
  
  private void loadEnumClass(String classIRI) throws Exception
  {
    if(classes.containsKey(classIRI))
      throw new Exception("class already created");
    new EnumClazz(this.getOntology(classIRI),classIRI);
  } 
  
  private Ontology getOntology(String iri)
  {
    String baseIri = iri.replaceAll("(.*[/#]).*","$1");
    Ontology toRet = this.ontologies.get(baseIri);
    if(toRet == null)
    {
      toRet = new Ontology(this,baseIri);
      this.ontologies.put(baseIri,toRet);
    }
    return toRet;
  }
  
  public void addClass(String iri, ClazzBase clazz)
  {
    this.classes.put(iri,clazz);
  }
  
  public String getPrefix(String prefix)
  {
    if(!prefix.endsWith(":"))
      prefix = prefix + ":";
    return this.con.expand(prefix);
  }  
  
  public Type getClass(String classIri)
  {
    return this.classes.get(classIri);
  }
  
  public HashMap<String, ClazzBase> getClasses()
  {
    return classes;
  }

  public String getCodeProjectName()
  {
    return codeProjectName;
  }

  public String getCodeProjectVersion()
  {
    return codeProjectVersion;
  }

  public String getCodeProjectFullName()
  {
    return codeProjectFullName;
  }

  public String getCodeProjectDescription()
  {
    return codeProjectDescription;
  }

  public RDFSimpleCon getCon()
  {
    return con;
  }

  public HashMap<String, Ontology> getOntologies()
  {
    return ontologies;
  }

  public String getCodeProjectGroup()
  {
    return codeProjectGroup;
  }  
  
  void addFieldToProperties(Field field)
  {
    Property prop = this.properties.get(field.getPredIRI());
    if(prop == null)
    {
      prop = new Property(field.getPredIRI());
      this.properties.put(field.getPredIRI(),prop);
    }
    prop.addField(field);
  }
  
  void addProperty(Property prop)
  {
    this.properties.put(prop.getPredIri(),prop);
  }

  public HashMap<String, Property> getProperties()
  {
    return properties;
  }  
  
  void addFrameRoot(Clazz clazz)
  {
    this.frameRoot.add(clazz);
  }

  public LinkedList<Clazz> getFrameRoot()
  {
    return frameRoot;
  }
}
