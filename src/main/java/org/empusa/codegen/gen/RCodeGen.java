package org.empusa.codegen.gen;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.empusa.codegen.Clazz;
import org.empusa.codegen.ClazzBase;
import org.empusa.codegen.CommandOptions;
import org.empusa.codegen.EnumClazz;
import org.empusa.codegen.EnumItem;
import org.empusa.codegen.Field;
import org.empusa.codegen.Ontology;
import org.empusa.codegen.OntologySet;
import org.empusa.codegen.OntologyVisitor;
import org.empusa.codegen.Type;
import org.empusa.codegen.type.BooleanType;
import org.empusa.codegen.type.DateTimeType;
import org.empusa.codegen.type.DateType;
import org.empusa.codegen.type.DoubleType;
import org.empusa.codegen.type.FloatType;
import org.empusa.codegen.type.IntegerType;
import org.empusa.codegen.type.LongType;

import nl.wur.ssb.RDFSimpleCon.Util;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

public class RCodeGen implements OntologyVisitor
{
  private OntologySet ontologySet;
  private String srcFolder;
  private CommandOptions args;
  private Writer packageFile;
  private CodeWriter packageMain;
  private List<String> enumInits = new LinkedList<String>();
  //private LinkedHashSet<Clazz> depDone = new LinkedHashSet<Clazz>();
  
  public RCodeGen(OntologySet ontology,CommandOptions args)
  {
    this.ontologySet = ontology;
    this.args = args;
    //build dir structure
    this.srcFolder = args.outRProjectDir + "/R/";
  }
  
  public void genCode() throws Exception
  {
    new File(this.srcFolder).mkdirs();
    new File(args.outRProjectDir + "/inst/java/").mkdirs();
    packageFile = new OutputStreamWriter(new FileOutputStream(new File(args.outRProjectDir + "/NAMESPACE")));
    packageFile.write("export(Domain)\n");
    packageFile.write("export(TypeMap)\n");
    List<String> packagesFieldDef = new LinkedList<String>();
    List<String> packagesInit = new LinkedList<String>();
    for(Ontology ontology : this.ontologySet.getOntologies().values())
    {
      String packageBase = OWLThingImpl.getBasePackage(ontology.getBaseIri());
      //life.gbol = "ANY"
      packagesFieldDef.add("    " + packageBase + " = \"ANY\"");
      //life.gbol <<- life.gbol$new(domain)
      packagesInit.add("      " + packageBase + " <<- C_" + packageBase + "$new(inner)");
      packageFile.write("export(C_" + packageBase + ")\n");
      
      /*
        life.gbol<- setRefClass(
          "life.gbol",
          field = list(
            domain = "ANY"
          ),
          methods = list(
            initialize = function(domainIn)
            {
              domain <<- domainIn
            },
            createActivty = function(iri)
            {
              life.gbol.Activity$new(domain,iri)
            }
          )
        )
       */
      packageMain = new CodeWriter(new OutputStreamWriter(new FileOutputStream(this.srcFolder + "/" + packageBase.replace(".","_") + ".R")),"  ");
      packageMain.indent("C_%s<- setRefClass(",packageBase);
      packageMain.writeln("\"%s\",",packageBase);
      packageMain.indent("field = list(");
      packageMain.writeln("domain = \"ANY\"");
      packageMain.deindent("),");
      packageMain.indent("methods = list(");
      packageMain.writeln("initialize = function(domainIn)");
      packageMain.indent("{");
      packageMain.writeln("domain <<- domainIn");
      packageMain.deindent("}");
      
      for(ClazzBase clazz : ontology.getClasses().values())
      {
        clazz.accept(this);
      }      
      
      packageMain.deindent(")");
      packageMain.deindent(")");
      packageMain.close();
    }
    /*String imports = "";
    for(Clazz clazz : this.depDone)
    {
      String fullName = OWLThingImpl.getBasePackage(clazz.getClassIRI()) + "." + clazz.getName();
      imports += String.format("source(\"./R/classes/%s.R\")\n",fullName.replace(".","_"));
    }*/
    FileUtils.write(new File(this.srcFolder + "/Domain.R"),String.format(Util.readFile("template/Domain.R"),
        StringUtils.join(packagesFieldDef,",\n"),
        StringUtils.join(packagesInit,"\n"),
        StringUtils.join(this.enumInits,"\n  ")));
    packageFile.close();
    FileUtils.write(new File(this.args.outRProjectDir + "/DESCRIPTION"),Util.readFile("template/DESCRIPTION"));

  }
  
  /*public void checkParentTree(Clazz clazz) 
  {
    for(Clazz parent : clazz.getParents())
    {
      checkParentTree(parent);
    }
    if(!this.depDone.contains(clazz))
      this.depDone.add(clazz);
  }*/
  
  @Override
  public void visitClass(Clazz clazz) throws Exception
  {
    /*
    if(!exists("foo")) source("./R/util.R") 
    
    Activity<- setRefClass(
        "Activity",
        contains = list("ClassATest"),
        fields = list(
          obj = "ANY"
        ),
        methods = list(
          initialize = function(domainIn,iri)
          {
            obj <<- .jcall(domain$domain,"Lnl.wur.ssb.RDFSimpleCon.api.OWLThing;","gbol.live.Activity",iri)
            domain <<- domainIn
            domainIn$addObj(.self)
          }
        )
      )
      
    assgin("http://x",Activity,TypeMap)
      */
    
    //checkParentTree(clazz);
       
    String fullName = OWLThingImpl.getBasePackage(clazz.getClassIRI()) + "." + clazz.getName();
    CodeWriter writer = new CodeWriter(new OutputStreamWriter(new FileOutputStream(this.srcFolder + "/" + fullName.replace(".","_") + ".R")),"  ");
    writer.writeln("if(!exists(\"TypeMap\")) source(\"./R/Domain.R\")"); 
    for(Clazz parent : clazz.getParents())
    {
      String parentFullName = OWLThingImpl.getBasePackage(parent.getClassIRI()) + "." + parent.getName();
      writer.writeln("if(!exists(\"%s\")) source(\"./R/%s.R\")",parentFullName, parentFullName.replace(".","_")); 
    }
    
    writer.indent(fullName + " <- setRefClass(");
    writer.writeln("\"%s\",",fullName);
    //writer.indent("fields = list(");
    //writer.writeln("obj = \"ANY\"");
    //writer.deindent(" ),");
    
    List<String> parents = new LinkedList<String>();
    for(Clazz parent : clazz.getParents())
      parents.add(OWLThingImpl.getBasePackage(parent.getClassIRI()) + "." + parent.getName());
    if(clazz.getParents().size() == 0)
      parents.add("OWLThing");
    writer.writeln("contains = list(\"%s\"),",StringUtils.join(parents,"\",\""));    
    writer.indent("methods = list(");
    writer.writeln("initialize = function(domainIn,iri)");
    writer.indent("{");
    writer.writeln("obj <<- .jcall(domainIn$domain,\"Lnl/wur/ssb/RDFSimpleCon/api/OWLThing;\",\"make\",\"%s\",iri)",OWLThingImpl.getBasePackage(clazz.getClassIRI()) + ".domain." + clazz.getName());
    writer.writeln("domain <<- domainIn");
    writer.writeln("domainIn$addObj(.self)");
    writer.deindent("}");    
    for(Field field : clazz.getFields())
    {
      emitField(writer,true,field);
    }
    writer.deindent(")");
    writer.deindent(")");
    writer.writeln("assign(\"%s\",%s,TypeMap)",clazz.getClassIRI(),fullName);
    writer.close();
    
    packageMain.write(",create%s = function(iri)",clazz.getName());
    packageMain.indent("{");
    packageMain.writeln("%s$new(domain,iri)",OWLThingImpl.getBasePackage(clazz.getClassIRI()) + "." + clazz.getName());
    packageMain.deindent("}");
    
    packageFile.write("export(" + fullName + ")\n");
  }
  
  public void emitField(CodeWriter writer,boolean onlyInterfaceDef,Field field) throws Exception
  {
    if(field.isArray())
    {
      if(field.isSeq())
      {
        
        /*
          ,g_tProp1_1 = function(index) 
          {
            .jcall(obj,"Ltype;","getProp1_1",val,as.integer(index))
          }
        */
        writer.writeln(",g_t%s = function(index)",WordUtils.capitalize(field.getName()));
        writer.indent("{");
        writer.writeln(castFromJava(field,String.format(".jcall(obj,\"L%s;\",\"%s\",as.integer(index))",field.getClassType().toString().replace(".","/"),"get" + WordUtils.capitalize(field.getName()))));
        writer.deindent("}");
        
        /*
          ,s_tProp1_1 = function(val,index) 
          {
            .jcall(obj,"Ltype;","getProp1_1",val,as.integer(index))
          }
        */
        writer.writeln(",s_t%s = function(val,index)",WordUtils.capitalize(field.getName()));
        writer.indent("{");
        writer.writeln(".jcall(obj,\"V\",\"set%s\",%s,as.integer(index))", WordUtils.capitalize(field.getName()),castToJava(field));
        writer.deindent("}");
      }
     
      /*
        ,remProp1_1 = function(val) 
        {
          .jcall(obj,"Ltype;","remProp1_1",val))
        }
      */
      writer.writeln(",rem%s = function(val)",WordUtils.capitalize(field.getName()));
      writer.indent("{");
      writer.writeln(".jcall(obj,\"V\",\"rem%s\",%s)", WordUtils.capitalize(field.getName()),castToJava(field));
      writer.deindent("}");
      
  
      /*
        ,addProp1_1 = function(val) 
        {
          .jcall(obj,"Ltype;","addProp1_1",val))
        }
      */
      writer.writeln(",add%s = function(val)",WordUtils.capitalize(field.getName()));
      writer.indent("{");
      writer.writeln(".jcall(obj,\"V\",\"add%s\",%s)", WordUtils.capitalize(field.getName()),castToJava(field));
      writer.deindent("}");
              
      /*
        ,getAllProp1_1 = function() 
        {
          theList <- .jcall(gene1$obj,"Ljava/util/List;","getAllExon")
          walker <- .jcall(theList,"Ljava/util/Iterator;","iterator")
          toRet <- c()
          while(.jcall(walker,"Z","hasNext"))
          {
            toRet <- c(toRet,.jcall(walker,"Ljava/lang/Object;","next"))
          }
          toRet
        }
      */
      writer.writeln(",getAll%s = function(val)",WordUtils.capitalize(field.getName()));
      writer.indent("{");
      writer.writeln("theList <- .jcall(obj,\"Ljava/util/List;\",\"getAll%s\")", WordUtils.capitalize(field.getName()));
      writer.writeln("walker <- .jcall(theList,\"Ljava/util/Iterator;\",\"iterator\")");
      writer.writeln("toRet <- c()");
      writer.writeln("while(.jcall(walker,\"Z\",\"hasNext\"))");
      writer.indent("{");
      writer.writeln("toRet <- c(toRet,%s)",castFromJava(field,".jcall(walker,\"Ljava/lang/Object;\",\"next\")"));
      writer.deindent("}");
      writer.writeln("toRet");
      writer.deindent("}");      

    }
    else
    {
      /*
        ,getProp1_1 = function() 
        {
          .jcall(obj,"Ltype;","getProp1_1")
        }
      */
      writer.writeln(",%s = function()","get" + WordUtils.capitalize(field.getName()));
      writer.indent("{");
      writer.writeln(castFromJava(field,String.format(".jcall(obj,\"L%s;\",\"%s\")",field.getClassType().toString().replace(".","/"),"get" + WordUtils.capitalize(field.getName()))));
      writer.deindent("}");
      /*
          ,setProp1_1 = function(val) 
          {
            .jcall(obj,"Ltype;","setProp1_1",val)          
          }
      */
      writer.writeln(",%s = function(val)","set" + WordUtils.capitalize(field.getName()));
      writer.indent("{");
      writer.writeln(".jcall(obj,\"V\",\"%s\",%s)","set" + WordUtils.capitalize(field.getName()),castToJava(field));
      writer.deindent("}");
    }
  }
  
  private String castFromJava(Field field,String inner)
  {    
    Type type = field.getType();
    if(type instanceof Clazz)
      return String.format("domain$getObj(%s)",inner);
    else if(type instanceof IntegerType)
      return String.format("decodeInt(%s)",inner);
    else if(type instanceof LongType)
      return String.format("decodeLong(%s)",inner);
    else if(type instanceof DateTimeType) 
      return String.format("as.POSIXct(.jcall(.jcall(%s,\"Ljava/time/Instant;\",\"toInstant\",.jcall(.jcall(J(\"java.time.ZoneId\")$systemDefault(),\"Ljava/time/zone/ZoneRules;\",\"getRules\"),\"Ljava/time/ZoneOffset;\",\"getOffset\",intest)),\"J\",\"getEpochSecond\"),origin = \"1970-01-01\")",inner);
    else if(type instanceof DateType)
      return String.format("as.Date(.jcall(%s,\"J\",\"toEpochDay\"),origin=\"1970-01-01\")",inner);
    else if(type instanceof FloatType || type instanceof DoubleType)
      return String.format("as.numeric(%s)",inner);
    else if(type instanceof BooleanType)
      return String.format("as.logical(%s)",inner);
    else
    {
      return inner;
    }
  }

  private String castToJava(Field field)
  {
    /*
     * REF ok
     * Enum ok
     * integer ok
     * String ok
     * ExternalRef = String ok
     * Date ok
     * DateTime ok 
     * Double testit
     * Boolean testit
     * Float testit
     */


    
    Type type = field.getType();
    if(type instanceof Clazz)
      return String.format(".jcast(val$obj,\"%s\")",field.getClassType().toString());
    else if(type instanceof DateTimeType) 
      return "J(\"java.time.LocalDateTime\")$ofInstant(J(\"java.time.Instant\")$ofEpochSecond(.jlong(val)),J(\"java.time.ZoneId\")$systemDefault())";
    else if(type instanceof DateType)
      return "J(\"java.time.LocalDate\")$ofEpochDay(.jlong(val))";
    else if(type instanceof IntegerType)
      return ".jnew(\"java.lang.Integer\",as.integer(val))";
    else if(type instanceof LongType)
      return ".jnew(\"java.lang.Long\",.jlong(val))";
    else if(type instanceof FloatType)
      return ".jnew(\"java.lang.Float\",.jfloat(val))";
    else if(type instanceof DoubleType)
      return ".jnew(\"java.lang.Double\",as.numeric(val))";
    else if(type instanceof BooleanType)
      return ".jnew(\"java.lang.Boolean\",as.logic(val))";
    else
      return String.format("val");
  }

  @Override
  public void visitEnumClass(EnumClazz clazz) throws Exception
  {    
    /*
    life.gbol.StrandPosition <- NA
    load_life.gbol.StrandPosition <- function()
    {
      toSet <- data.frame(matrix(NA, nrow=1, ncol=3))
      names(toSet) <- c("ReverseStrandPosition","ForwardStrandPosition","BothStrandsPosition")
      toSet$ReverseStrandPosition <- J("life.gbol.domain.StrandPosition")$ReverseStrandPosition
      toSet$ForwardStrandPosition <- J("life.gbol.domain.StrandPosition")$ForwardStrandPosition
      toSet$BothStrandsPosition <- J("life.gbol.domain.StrandPosition")$BothStrandsPosition
      unlockBinding("life.gbol.StrandPosition",as.environment("package:RGBOLApi"))
      assign("life.gbol.StrandPosition",toSet,as.environment("package:RGBOLApi"))
      lockBinding("life.gbol.StrandPosition",as.environment("package:RGBOLApi"))
    }    
    */
    String fullName = OWLThingImpl.getBasePackage(clazz.getClassIRI()) + "." + clazz.getName();
    CodeWriter writer = new CodeWriter(new OutputStreamWriter(new FileOutputStream(this.srcFolder + "/" + fullName.replace(".","_") + ".R")),"  ");
    
    writer.writeln("%s <- NA",fullName);
    writer.writeln("load_%s <- function()",fullName);
    writer.indent("{");
    writer.writeln("toSet <- data.frame(matrix(NA, nrow=1, ncol=%s))","" + clazz.getItems().size());
    writer.writeln("names(toSet) <- c(\"%s\")",
        StringUtils.join(clazz.getItems().values().stream().map(item -> item.getName()).toArray(),"\",\""));
    for(EnumItem item : clazz.getItems().values())
    {
      writer.writeln("toSet$%s <- J(\"%s\")$%s",item.getName(),
          OWLThingImpl.getBasePackage(clazz.getClassIRI()) + ".domain." + clazz.getName(),item.getName());
    }
    String packageName = this.args.outRProjectDir.replaceAll(".*/","");
    writer.writeln("unlockBinding(\"%s\",as.environment(\"package:%s\"))",fullName,packageName);
    writer.writeln("assign(\"%s\",toSet,as.environment(\"package:%s\"))",fullName,packageName);
    writer.writeln("lockBinding(\"%s\",as.environment(\"package:%s\"))",fullName,packageName);
    writer.deindent("}");
    enumInits.add(String.format("load_%s()",fullName));

    writer.close();    
    packageFile.write("export(" + fullName + ")\n");
  } 
}
