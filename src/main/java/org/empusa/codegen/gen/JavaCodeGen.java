package org.empusa.codegen.gen;

import com.squareup.javapoet.*;
import com.squareup.javapoet.TypeSpec.Builder;
import nl.wur.ssb.RDFSimpleCon.Util;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.jena.rdf.model.Resource;
import org.empusa.codegen.*;

import javax.lang.model.element.Modifier;
import java.io.File;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;

public class JavaCodeGen implements OntologyVisitor
{
  private OntologySet ontologySet;
  private String srcFolder;
  private CommandOptions args;
  
  public JavaCodeGen(OntologySet ontology,CommandOptions args)
  {
    this.ontologySet = ontology;
    this.args = args;
    //build dir structure
    this.srcFolder = args.outProjectDir + "/src/main/java/";
  }
  
  public void genCode() throws Exception
  {
    buildBaseProject();
    for(ClazzBase clazz : this.ontologySet.getClasses().values())
    {
      clazz.accept(this);
    }
  }
  
  private void buildBaseProject() throws Exception
  {
    //get base iri from ontology file
    //for(Ontology )

    /*
     *  <groupId>%1$s</groupId>
        <artifactId>%1$s</artifactId>
        <version>%2$s</version>
        <name>%3$s</name>
        <description>%4$s</description>
        <mainClass>%5$s</mainClass>
     */
    if(!args.excludeBaseFiles)
    {
      FileUtils.write(new File(this.args.outProjectDir + "/build.gradle"),String.format(Util.readFile("template/build.gradle"),ontologySet.getCodeProjectGroup(),ontologySet.getCodeProjectVersion(),ontologySet.getCodeProjectName(),ontologySet.getCodeProjectDescription()));
      FileUtils.write(new File(this.args.outProjectDir + "/settings.gradle"),String.format(Util.readFile("template/settings.gradle"),ontologySet.getCodeProjectName()));   
      
      //to be deprecated
      //FileUtils.write(new File(this.args.outProjectDir + "/pom.xml"),String.format(Util.readFile("template/pom.xml"),ontologySet.getCodeProjectName(),ontologySet.getCodeProjectVersion(),ontologySet.getCodeProjectFullName(),ontologySet.getCodeProjectDescription()));
      //FileUtils.write(new File(this.args.outProjectDir + "/.classpath"),Util.readFile("template/classpath"));
      //FileUtils.write(new File(this.args.outProjectDir + "/.project"),String.format(Util.readFile("template/project"),ontologySet.getCodeProjectName(),ontologySet.getCodeProjectDescription()));
      File installSh = new File(this.args.outProjectDir + "/install.sh");

      String rCodeCopy = "";
      if(args.outRProjectDir != null)
      {
        String base = "";
        for(int i = 0;i < args.outProjectDir.length() && i < args.outRProjectDir.length();i++)
        {
          if(args.outProjectDir.charAt(i) != args.outRProjectDir.charAt(i))
          {
            base = "../" + args.outRProjectDir.substring(i);
            break;
          }
        }
        rCodeCopy = "if [ -d $DIR/" + base + " ]; then\n";
        rCodeCopy += "  cp $DIR/" + ontologySet.getCodeProjectName() + ".jar $DIR/" + base + "/inst/java/\n";
        rCodeCopy += "fi\n";
      }

      FileUtils.write(installSh,String.format(Util.readFile("template/install.sh"),ontologySet.getCodeProjectName(),rCodeCopy));
      installSh.setExecutable(true);
    }
  }
  
  
   
  @Override
  public void visitClass(Clazz clazz) throws Exception
  {
    this.generateClazz(clazz);
    this.generateInterface(clazz);
  }
  
  public void generateInterface(Clazz clazz) throws Exception
  {
    Builder buildClazz = TypeSpec.interfaceBuilder(clazz.getName()).addModifiers(Modifier.PUBLIC).addJavadoc("Code generated from " + clazz.getOntology().getBaseIri() + " ontology\n");
    
    for(Clazz parent : clazz.getParents())
      buildClazz.addSuperinterface(ClassName.get(OWLThingImpl.getBasePackage(parent.getClassIRI()) + ".domain",parent.getName()));
    if(clazz.getParents().size() == 0)
      buildClazz.addSuperinterface(ClassName.get("nl.wur.ssb.RDFSimpleCon.api","OWLThing"));
    
    for(Field field : clazz.getFields())
    {
      emitField(buildClazz,true,field);
    }
    
    JavaFile.builder(OWLThingImpl.getBasePackage(clazz.getClassIRI()) + ".domain",buildClazz.build()).build().writeTo(new File(this.srcFolder));
  }
  
  public void generateClazz(Clazz clazz) throws Exception
  {  
    ClassName clazzName = ClassName.get(OWLThingImpl.getBasePackage(clazz.getClassIRI()) + ".domain.impl",clazz.getName() + "Impl");
    ClassName interfaceName = ClassName.get(OWLThingImpl.getBasePackage(clazz.getClassIRI()) + ".domain",clazz.getName());
    
    
    Builder clazzBuilder = TypeSpec.classBuilder(clazz.getName() + "Impl").addModifiers(Modifier.PUBLIC).addJavadoc("Code generated from " + clazz.getOntology().getBaseIri() + " ontology\n");
    
    // The parent classes subclass from OWLThing so we will have acces to the
    // helper functions in all objects
    // Extends from the first class, reimplement the methods from the other
    if(clazz.getParents().size() != 0)
      clazzBuilder.superclass(ClassName.get(OWLThingImpl.getBasePackage(clazz.getParents().getFirst().getClassIRI()) + ".domain.impl",clazz.getParents().getFirst().getName() + "Impl"));
    else
      clazzBuilder.superclass(ClassName.get("nl.wur.ssb.RDFSimpleCon.api" ,"OWLThingImpl"));
    clazzBuilder.addSuperinterface(ClassName.get(OWLThingImpl.getBasePackage(clazz.getClassIRI()) + ".domain",clazz.getName()));
    
    /*
     *   public static final String TypeIRI = "http://example.com/Example";
     */
    clazzBuilder.addField(FieldSpec.builder(String.class,"TypeIRI",Modifier.PUBLIC,Modifier.STATIC,Modifier.FINAL).initializer("$S",clazz.getClassIRI()).build());
    
    /*
     * protected Example(Domain domain,Resource resource) { super(domain,resource); }
     */
    
    clazzBuilder.addMethod(MethodSpec.constructorBuilder().addModifiers(Modifier.PROTECTED).addParameter(ClassName.get("nl.wur.ssb.RDFSimpleCon.api","Domain"),"domain")
        .addParameter(Resource.class,"resource").addStatement("super(domain,resource)").build());
    
    /*
     * public static Example make(Domain domain,Resource resource,boolean direct) 
     * {
          synchronized(domain) {
            Object toRet = null;
            if(direct)
              toRet = new ExampleImpl(domain,resource);
            else
            { 
              domain.getObject(resource,Example.class);
              if(toRet == null) {
                toRet = domain.getObjectFromResource(resource,Example.class,false);
                if(toRet == null)
                  toRet = new ExampleImpl(domain,resource);
              }
              else if(!(toRet instanceof Example)) {
                throw new RuntimeException("Instance of nl.wur.ssb.owlshex.test.domain.impl.ExampleImpl expected");
              }
            }
            return (Example)toRet;
          }
     *  }
     */
    
    clazzBuilder.addMethod(MethodSpec.methodBuilder("make")
        .addModifiers(Modifier.PUBLIC)
        .addModifiers(Modifier.STATIC)
        .returns(interfaceName)
        .addParameter(ClassName.get("nl.wur.ssb.RDFSimpleCon.api","Domain"),"domain")
        .addParameter(Resource.class,"resource")
        .addParameter(boolean.class,"direct")
        .beginControlFlow("synchronized(domain)")
        .addStatement("Object toRet = null")
        .beginControlFlow("if(direct)")
        .addStatement("toRet = new $T(domain,resource);",clazzName)
        .endControlFlow()
        .beginControlFlow("else")
        .addStatement("toRet = domain.getObject(resource,$T.class)",interfaceName)
        .beginControlFlow("if(toRet == null)")
        .addStatement("toRet = domain.getObjectFromResource(resource,$T.class,false)",interfaceName)
        .beginControlFlow("if(toRet == null)")
        .addStatement("toRet = new $T(domain,resource);",clazzName)
        .endControlFlow()
        .endControlFlow()
        .beginControlFlow("else if(!(toRet instanceof $T))",interfaceName)
        .addStatement("throw new RuntimeException($S)","Instance of " + clazzName + " expected")
        .endControlFlow()
        .endControlFlow()
        .addStatement("return ($T)toRet",interfaceName)
        .endControlFlow().build());
    
    /*
     * public void validate() 
     * {
     *   this.checkCardMin1("http://example.com/prop1_1"); 
     * }
     */
    MethodSpec.Builder mBuilder = MethodSpec.methodBuilder("validate").addModifiers(Modifier.PUBLIC);
    
    mBuilder.addStatement("super.validate()");
    for(Field field : clazz.getFields())
    {
      if(!field.isOptional())
        mBuilder.addStatement("this.checkCardMin1($S)",field.getPredIRI());
    }
    clazzBuilder.addMethod(mBuilder.build());
    
    LinkedHashMap<String,Field> implFields = new LinkedHashMap<String,Field>();
    prepareFields(clazz,implFields);
    for(Field field : implFields.values())
    {
      this.emitField(clazzBuilder,false,field);
    }
    
    JavaFile.builder(OWLThingImpl.getBasePackage(clazz.getClassIRI()) + ".domain.impl",clazzBuilder.build()).build().writeTo(new File(this.srcFolder));
  }
  
  private void prepareFields(Clazz clazz,LinkedHashMap<String,Field> implFields) throws Exception
  {
    for(Field field : clazz.getFields())
    {
      addField(field,implFields);
    }
    for(Clazz parent : clazz.getParents())
    {
      this.prepareFields(parent,implFields);
    }
  }
  
  private void addField(Field field,LinkedHashMap<String,Field> implFields) throws Exception
  {
    Field current = implFields.get(field.getName());
    if(current != null)
    {
      if(!field.getPredIRI().equals(current.getPredIRI()))
        throw new Exception("Two field with the same name, but with different iris: " + field.getPredIRI() + " <-> " + current.getPredIRI());
      if(current.getType().matches(field.getType()))
      {
        //keep current field
        return;
      }
      else if(field.getType().matches(current.getType()))
      {
        implFields.put(field.getName(),field);
      }
      else
      {
        throw new Exception("Two mismatching types on field: " + field.getPredIRI() + " | " + current.getType() + " <-> " + field.getType());
      }
    }
    else
    {
      implFields.put(field.getName(),field);
    }
  } 
  
  public void emitField(Builder clazz,boolean onlyInterfaceDef,Field field)
  {
    if(field.isArray())
    {
      if(field.isSeq() || field.isList())
      {
        String listAppend = field.isList() ? "List" : "";
        /*
        public List<? extends Example> getProp0_1(int index)
        { 
          return this.getRefListAtIndex("http://example.com/propRefList0_1",true,Example.class,index);
        }
        */
        MethodSpec.Builder method = MethodSpec.methodBuilder("get" + WordUtils.capitalize(field.getName()))
            .addModifiers(Modifier.PUBLIC)
            .addParameter(int.class,"index")
            .returns(field.getClassType());
        if(!onlyInterfaceDef)
            method.addStatement("return this.get$NListAtIndex$N($S,$N,$T.class,index)",field.getClassTypeName(),listAppend,field.getPredIRI(),"" + field.isOptional(),field.getClassType());
        else
          method.addModifiers(Modifier.ABSTRACT);
        
        clazz.addMethod(method.build());
        
        /*
        public List<? extends Example> getAllProp0_1()
        {
          return this.getRefList("http://example.com/propRefList0_1",true,Example.class);
        }
        */
        method = MethodSpec.methodBuilder("getAll" + WordUtils.capitalize(field.getName()))
            .addModifiers(Modifier.PUBLIC)
            .returns(ParameterizedTypeName.get(ClassName.get("java.util","List"),WildcardTypeName.subtypeOf(field.getClassType())));
        if(!onlyInterfaceDef)
          method.addStatement("return this.get$NList$N($S,$N,$T.class)",field.getClassTypeName(),listAppend,field.getPredIRI(),"" + field.isOptional(),field.getClassType());
        else
          method.addModifiers(Modifier.ABSTRACT);

        clazz.addMethod(method.build());
        
        /*
        public void addRefListProp0_1(Example val)
        {
          this.addRefList("http://example.com/propRefList0_1",val);
        }
        */
        method = MethodSpec.methodBuilder("add" + WordUtils.capitalize(field.getName()))
        .addModifiers(Modifier.PUBLIC)
        .addParameter(field.getClassType(), "val");
        if(!onlyInterfaceDef)
          method.addStatement("this.add$NList$N($S,val)",field.getClassTypeName(),listAppend,field.getPredIRI());
        else
          method.addModifiers(Modifier.ABSTRACT);
        
        clazz.addMethod(method.build());
        
        /*
        public void setProp0_1(Example val,int index)
        {
          this.setRefList("http://example.com/propRefList0_1",val,true,index);
        }
        */
        method = MethodSpec.methodBuilder("set" + WordUtils.capitalize(field.getName()))
        .addModifiers(Modifier.PUBLIC)
        .addParameter(field.getClassType(), "val")
        .addParameter(int.class,"index");
        if(!onlyInterfaceDef)
          method.addStatement("this.set$NList$N($S,val,$N,index)",field.getClassTypeName(),listAppend,field.getPredIRI(),"" + field.isOptional());
        else
          method.addModifiers(Modifier.ABSTRACT);
        
        clazz.addMethod(method.build());

        /*
        public void remRefListProp0_1(Example val)
        {
          this.remRefList("http://example.com/propRefList0_1",val,true);
        }
        */
        method = MethodSpec.methodBuilder("rem" + WordUtils.capitalize(field.getName()))
        .addModifiers(Modifier.PUBLIC)
        .addParameter(field.getClassType(), "val");
        if(!onlyInterfaceDef)
          method.addStatement("this.rem$NList$N($S,val,$N)",field.getClassTypeName(),listAppend,field.getPredIRI(),"" + field.isOptional());
        else
          method.addModifiers(Modifier.ABSTRACT);
        
        clazz.addMethod(method.build());
      }
      else
      {
        /*                
          public void remProp1_N(String val)
          {
            this.remStringLit("http://example.com/prop1_N",val,false);
          }
         */
        MethodSpec.Builder method = MethodSpec.methodBuilder("rem" + WordUtils.capitalize(field.getName()))
        .addModifiers(Modifier.PUBLIC)
        .addParameter(field.getClassType(), "val");
        if(!onlyInterfaceDef)
          method.addStatement("this.rem$N($S,val,$N)",field.getClassTypeName(),field.getPredIRI(),"" + field.isOptional());
        else
          method.addModifiers(Modifier.ABSTRACT);
        
        clazz.addMethod(method.build());
        
        /*
          public List<? extends String> getProp1_N()
          {
            return this.getStringLitSet("http://example.com/prop1_N",false);
          }
         */
        method = MethodSpec.methodBuilder("getAll" + WordUtils.capitalize(field.getName()))
            .addModifiers(Modifier.PUBLIC)
            .returns(ParameterizedTypeName.get(ClassName.get("java.util","List"),WildcardTypeName.subtypeOf(field.getClassType())));
        if(!onlyInterfaceDef)
        {
          if(field.needExpectedType())
            method.addStatement("return this.get$NSet($S,$N,$T.class)",field.getClassTypeName(),field.getPredIRI(),"" + field.isOptional(),field.getClassType());
          else
            method.addStatement("return this.get$NSet($S,$N)",field.getClassTypeName(),field.getPredIRI(),"" + field.isOptional());
        }
        else
          method.addModifiers(Modifier.ABSTRACT);
        
        clazz.addMethod(method.build());
        
        /*        
          public void addProp1_N(String val)
          {
             this.addStringLit("http://example.com/prop1_N",val);
          }  
        */
        method = MethodSpec.methodBuilder("add" + WordUtils.capitalize(field.getName()))
            .addModifiers(Modifier.PUBLIC)
            .addParameter(field.getClassType(), "val");
        if(!onlyInterfaceDef)
            method.addStatement("this.add$N($S,val)",field.getClassTypeName(),field.getPredIRI());
        else
          method.addModifiers(Modifier.ABSTRACT);
        
        clazz.addMethod(method.build());
      }
    }
    else
    {
      /*
        public String getProp1_1() 
        {
          return this.getStringLit("http://example.com/prop1_1",false);
        }
      */
      MethodSpec.Builder method = MethodSpec.methodBuilder("get" + WordUtils.capitalize(field.getName()))
          .addModifiers(Modifier.PUBLIC)
          .returns(field.getClassType());
      if(!onlyInterfaceDef)
      {
        if(field.needExpectedType())
          method.addStatement("return this.get$N($S,$N,$T.class)",field.getClassTypeName(),field.getPredIRI(),"" + field.isOptional(),field.getClassType());
        else
          method.addStatement("return this.get$N($S,$N)",field.getClassTypeName(),field.getPredIRI(),"" + field.isOptional());
      }
      else
        method.addModifiers(Modifier.ABSTRACT);
        
        clazz.addMethod(method.build());
        /*
        public void setProp1_1(String val)
        {
          this.setStringLit("http://example.com/prop1_1",val);
        }
      */
      method = MethodSpec.methodBuilder("set" + WordUtils.capitalize(field.getName()))
            .addModifiers(Modifier.PUBLIC)
            .addParameter(field.getClassType(), "val");
      if(!onlyInterfaceDef)
      {
        if(field.needExpectedType())
          method.addStatement("this.set$N($S,val,$T.class)",field.getClassTypeName(),field.getPredIRI(),field.getClassType());
        else
          method.addStatement("this.set$N($S,val)",field.getClassTypeName(),field.getPredIRI());
      }
      else
        method.addModifiers(Modifier.ABSTRACT);
      clazz.addMethod(method.build());
    }
  }
  
  public void visitEnumClass(EnumClazz clazz) throws Exception
  {   
    ClassName clazzName = ClassName.get(OWLThingImpl.getBasePackage(clazz.getClassIRI()) + ".domain",clazz.getName());
    Builder clazzBuilder = TypeSpec.enumBuilder(clazz.getName())
        .addModifiers(Modifier.PUBLIC)
        .addSuperinterface(ClassName.get("nl.wur.ssb.RDFSimpleCon.api","EnumClass"))
        .addJavadoc("Code generated from " + clazz.getOntology().getBaseIri() + " ontology\n");
    
    /*
      TEST1("http://example.com/TEST1",null), 
      TEST2("http://example.com/TEST2",new EnumExample[]{TEST1}), 
      TEST3("http://example.com/TEST3",new EnumExample[]{TEST2}), 
      TEST4("http://example.com/TEST4",new EnumExample[]{TEST3});
     */
    HashSet<EnumItem> emitted = new HashSet<EnumItem>();
    for(EnumItem item : clazz.getItems().values())
    {
      emitEnumItem(item,clazzBuilder,clazz,emitted,clazzName);
    }
    /*
     * private EnumExample parents[];
     * private String iri;
     */
   
    clazzBuilder.addField(FieldSpec.builder(ArrayTypeName.of(clazzName),"parents",Modifier.PRIVATE).build());
    clazzBuilder.addField(FieldSpec.builder(String.class,"iri",Modifier.PRIVATE).build());
    
    /*
      private EnumExample(String iri,EnumExample[] parents)
      {
        this.iri = iri;
        this.parents = parents;
      }
    */
    
    clazzBuilder.addMethod(MethodSpec.constructorBuilder()
    .addModifiers(Modifier.PRIVATE)
    .addParameter(String.class,"iri")
    .addParameter(ArrayTypeName.of(clazzName), "parents")
    .addStatement("this.iri = iri")
    .addStatement("this.parents = parents")
    .build());  

    /*
        public EnumClass[] getParents()
        {
          return parents;
        }
     */
    
    clazzBuilder.addMethod(MethodSpec.methodBuilder("getParents")
    .addModifiers(Modifier.PUBLIC)
    .returns(ArrayTypeName.of(ClassName.get("nl.wur.ssb.RDFSimpleCon.api","EnumClass")))
    .addStatement("return parents")
    .build());
      
    /*
    public String getIRI()
    {
      return this.iri;
    }
    */
    
    clazzBuilder.addMethod(MethodSpec.methodBuilder("getIRI")
    .addModifiers(Modifier.PUBLIC)
    .returns(String.class)
    .addStatement("return this.iri")
    .build());
    
    /*
    public static EnumExample make(String iri)
    {
      for(EnumExample item : EnumExample.values())
      {
        if(item.iri.equals(iri))
          return item;
      }
      throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
    } 
    */
    
    clazzBuilder.addMethod(MethodSpec.methodBuilder("make")
    .addModifiers(Modifier.PUBLIC)
    .addModifiers(Modifier.STATIC)
    .addParameter(String.class, "iri")
    .returns(clazzName)
    .beginControlFlow("for($T item : $T.values())",clazzName,clazzName)
    .beginControlFlow("if(item.iri.equals(iri))")
    .addStatement("return item")
    .endControlFlow()
    .endControlFlow()
    .addStatement("throw new RuntimeException(\"Enum value: \" + iri + \" not found for enum type: EnumExample\")")
    .build());  
    
    JavaFile.builder(OWLThingImpl.getBasePackage(clazz.getClassIRI()) + ".domain", clazzBuilder.build()).build().writeTo(new File(this.srcFolder));
  }
  
  private void emitEnumItem(EnumItem item, Builder clazzBuilder,EnumClazz clazz, HashSet<EnumItem> emitted,ClassName clazzName ) throws Exception
  {
    if(emitted.contains(item))
      return;
    for(EnumItem parent : item.getParents())
      emitEnumItem(parent,clazzBuilder,clazz,emitted,clazzName);
    
    LinkedList<Object> tmp = new LinkedList<Object>();
    tmp.add(item.getClassIRI());
    tmp.add(clazzName);
    for(EnumItem parent : item.getParents())
      tmp.add(parent.getName());  
    clazzBuilder.addEnumConstant(encodeToJavaType(item.getName()),TypeSpec.anonymousClassBuilder("$S,new $T[]{" + StringUtils.repeat("$N",",",item.getParents().size()) +"}",tmp.toArray()).build());
    
    emitted.add(item);
  }
  
  private String encodeToJavaType(String name)
  {
    return name.replace("-","_");
  }
  
  public String getSrcFolder()
  {
    return srcFolder;
  }

}
