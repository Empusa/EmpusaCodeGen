package org.empusa.codegen.gen;

import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.empusa.RDF2Graph.domain.ClassProperty;
import org.empusa.RDF2Graph.domain.Multiplicity;
import org.empusa.RDF2Graph.domain.TypeLink;
import org.empusa.codegen.*;
import org.empusa.codegen.type.ExternalRefType;
import org.w3.rdfsyntaxns.domain.Property;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;

public class RDF2GraphGen implements OntologyVisitor
{
  private OntologySet ontology;
  private CommandOptions args;
  private Domain domain;
  private HashMap<String,LinkedList<String>> keepUniq = new HashMap<String,LinkedList<String>>(); 
  
  
  public RDF2GraphGen(OntologySet ontology,CommandOptions args)
  {
    this.ontology = ontology;
    this.args = args;
  }
  
  public void gen() throws Exception
  {
    RDFSimpleCon con = new RDFSimpleCon("");
    con.addAllPrefixes(this.ontology.getCon());  
    this.domain = new Domain(con);
    for(ClazzBase clazz : this.ontology.getClasses().values())
    {
      clazz.accept(this);
    }
    File folder = new File(args.RDF2GraphOutFile).getParentFile();
    if (!folder.exists()) {
      folder.mkdirs();
    }
    con.save(args.RDF2GraphOutFile);
    con.close();
  }
  
  private boolean parentHasField(Clazz clazz,String iri)
  {
    for(Clazz parent : clazz.getParents())
    {
      if(parent.getFieldByIri(iri) != null)
        return true;
      if(parentHasField(parent,iri))
        return true;
    }
    return false;
  }
  
  @Override
  public void visitClass(Clazz clazz) throws Exception
  {
    org.empusa.RDF2Graph.domain.Class rdfClass = this.domain.make(org.empusa.RDF2Graph.domain.Class.class,clazz.getClassIRI());
    for(Clazz parent : clazz.getParents())
    {
      org.empusa.RDF2Graph.domain.Class parentClazz = this.domain.make(org.empusa.RDF2Graph.domain.Class.class,parent.getClassIRI());
      rdfClass.addSubClassOf(parentClazz);
    }
    for(Field field : clazz.getFields())
    {
      String iri = field.getPredIRI();
      if(args.skipNarrowingProperties && parentHasField(clazz,iri))
        continue;
      Property rdfProperty = this.domain.make(Property.class,iri);
      String classPropIri = buildUniqIri(clazz.getClassIRI(),iri);
      ClassProperty classProperty = this.domain.make(ClassProperty.class,classPropIri);
      TypeLink typeLink = this.domain.make(TypeLink.class,classPropIri + "/TypeLink");
      if(field.isArray())
      {
        if(field.isOptional())
          typeLink.setForwardMultiplicity(Multiplicity.Zero_or_many);
        else
          typeLink.setForwardMultiplicity(Multiplicity.One_or_many);
      }
      else
      {
        if(field.isOptional())
          typeLink.setForwardMultiplicity(Multiplicity.Zero_or_one);
        else
          typeLink.setForwardMultiplicity(Multiplicity.Zero_or_one);        
      }
      typeLink.setReverseMultiplicity(field.getReverseMultipicity());
      
      Type type = field.getType();
      org.empusa.RDF2Graph.domain.Type targetType = null;
      if(type instanceof Clazz)
      {
        targetType = this.domain.make(org.empusa.RDF2Graph.domain.Class.class,((Clazz)type).getClassIRI());
      }
      else if(type instanceof EnumClazz || type instanceof ExternalRefType)
      {
        targetType = this.domain.make(org.empusa.RDF2Graph.domain.Class.class,"http://ssb.wur.nl/RDF2Graph/externalref");
      }
      else 
      {
        targetType = this.domain.make(org.empusa.RDF2Graph.domain.DataType.class,"xsd:" + type.getName());
      }
      typeLink.setType(targetType);
      
      classProperty.setRdfProperty(rdfProperty);
      classProperty.addLinkTo(typeLink);
      rdfClass.addProperty(classProperty); 
    }    
  }
  
  private String buildUniqIri(String parentIri,String childIri)
  {
    String key = parentIri.replace('#','/') + childIri.substring(Math.max(childIri.replace('#','/').lastIndexOf('/'),0));
    String key2 = parentIri.replace('#','/') + childIri;
    LinkedList<String> list = keepUniq.get(key);
    if(list == null)
    {
      list = new LinkedList<String>();
      list.add(key2);
      keepUniq.put(key,list);
      return key;
    }
    int index = list.indexOf(key2);
    if(index == -1)
    {
      index = list.size();
      list.add(key2);
    }
    if(index > 0)
      key += "/" + index;
    return key;
  }
  

  @Override
  public void visitEnumClass(EnumClazz clazz) throws Exception
  {
    //Do nothing not supported    
  }
  
}
