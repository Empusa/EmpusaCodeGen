package org.empusa.codegen.gen;

import java.io.Writer;

import org.apache.commons.lang3.StringUtils;

public class CodeWriter
{
  private Writer out;
  private String indent;
  private boolean newLine = false;
  private int indentDepth = 0;
  public CodeWriter(Writer out,String indent)
  {
    this.out = out;
    this.indent = indent;
  }
  
  public void indent()
  {
    this.indentDepth++;
  }
  
  public void indent(String in,Object ... args) throws Exception
  {
    indent(String.format(in,args));
  }
  
  public void indent(String in) throws Exception
  {
    this.writeln(in);
    this.indent();
  }
  
  public void deindent()
  {
    this.indentDepth--;
    if(this.indentDepth < 0)
      throw new RuntimeException("indent depth < 0");
  }
  
  public void deindent(String in,Object ... args) throws Exception
  {
    deindent(String.format(in,args));
  }
  
  public void deindent(String in) throws Exception
  {
    this.deindent();
    this.writeln(in);
  }

  public void writeln(String in,Object ... args) throws Exception
  {
    writeln(String.format(in,args));
  }
  
  public void writeln(String in) throws Exception
  {
    write(in + "\n");
  }
  
  public void write(String in,Object ... args) throws Exception
  {
    write(String.format(in,args));
  }
  
  public void write(String in) throws Exception
  {
    String tmp[] = in.split("\n");
    for(int i = 0;i < tmp.length;i++)
    {
      if(newLine)
      {
        out.write("\n" + StringUtils.repeat(this.indent,this.indentDepth));          
      }
      out.write(tmp[i]);
      newLine = i + 1 < tmp.length;
    }
    if(in.endsWith("\n"))
    {
      newLine = true;
    }
  }

  public void close() throws Exception
  {
    if(this.indentDepth != 0)
      throw new RuntimeException("indent depth != 0 at file close");
    this.out.close();
  }
}
