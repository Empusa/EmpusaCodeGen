package org.empusa.codegen.gen;

import com.github.jsonldjava.utils.JsonUtils;
import org.apache.commons.io.FileUtils;
import org.empusa.codegen.*;
import org.empusa.codegen.type.XSDDataType;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;

public class JSONLDFrameGen implements OntologyVisitor
{
  private OntologySet ontology;
  private CommandOptions args;
  HashMap<String,Object> context = new HashMap<String,Object>();
  HashMap<String,HashSet<String>> doubleMap = new HashMap<String,HashSet<String>>();
  HashMap<String,String> reverseMap = new HashMap<String,String>();
  
  public JSONLDFrameGen(OntologySet ontology,CommandOptions args)
  {
    this.ontology = ontology;
    this.args = args;
  }
  
  public void gen() throws Exception
  {
    HashMap<String,Object> data = new HashMap<String,Object>();
    data.put("@context",context);

    for(ClazzBase clazz : this.ontology.getClasses().values())
    {
      clazz.accept(this);
    }
    for(Property prop : this.ontology.getProperties().values())
    {
      visitProperty(prop);
    }
    
    for(HashSet<String> item : this.doubleMap.values())
    {
      if(item.size() == 1)
      {
        String iri = item.iterator().next();
        Object obj = this.context.remove(iri);
        String name = getName(iri);
        this.context.put(getName(iri),obj);
        this.reverseMap.put(iri,name);
      }
      else
      {
        for(String subItem : item)
        {
          Object obj = this.context.remove(subItem);
          String shortForm = this.ontology.getCon().getShortForm(subItem);
          if(shortForm.startsWith(":"))
            shortForm = getName(subItem);
          this.context.put(shortForm,obj);
          this.reverseMap.put(subItem,shortForm);
        }
      }
    }
    
    
    /*
      "@type": ["PublishedGBOLDataSet","GBOLDataSet"],
      "organisms" : {
        "taxonomy" : {
          "provenance" : {
            "origin" : {
            }
          }
        }
      }
    */
    HashSet<Clazz> roots = new HashSet<Clazz>();
    for(Clazz item : this.ontology.getFrameRoot())
    {
      roots.add(item);
      roots.addAll(item.getAllChilds());
    }
    if(roots.size() != 0)
    {
      LinkedList<String> newRoots = new LinkedList<String>();
      for(Clazz root : roots)
      {
        newRoots.add(this.reverseMap.get(root.getClassIRI()));
      }
      Object rootSet = newRoots;
      if(newRoots.size() == 1)
        rootSet = newRoots.get(0);
      data.put("@type",rootSet);
    }
    data.putAll((HashMap<String,Object>)getFrameChilds(roots,null));
    this.context.putAll(this.ontology.getCon().getPrefixMap());
    FileUtils.write(new File(this.args.jsonFrameOutFile),JsonUtils.toPrettyString(data));
  }
  
  private Object getFrameChilds(HashSet<Clazz> sources,HashSet<Clazz> exclude)
  {    
    HashMap<String,Object> toRet = new HashMap<String,Object>();
    HashSet<Clazz> allsources = new HashSet<Clazz>();
    for(Clazz item : sources)
    {
      allsources.add(item);
      allsources.addAll(item.getAllChilds());
    }
    HashMap<Property,LinkedList<Field>> framingFields = new HashMap<Property,LinkedList<Field>>();
    for(Clazz item : allsources)
    {
      if(exclude != null && !exclude.contains(item))
        addTypeDef(toRet,this.reverseMap.get(item.getClassIRI()));
      for(Field field : item.getFramedFields())
      {
        LinkedList<Field> list = framingFields.get(field.getProperty());
        if(list == null)
        {
          list = new LinkedList<Field>();
          framingFields.put(field.getProperty(),list);
        }
        list.add(field);
      }
    }
    for(Property subProperty : framingFields.keySet())
    {
      HashSet<Clazz> newsources = new HashSet<Clazz>();
      for(Type type : subProperty.getRange())
      {
        if(type instanceof Clazz)
          newsources.add((Clazz)type);
      }
      HashSet<Clazz> excludeNew = new HashSet<Clazz>();
      for(Field field : framingFields.get(subProperty))
        excludeNew.addAll(field.getExludeTypeInFraming());
      toRet.put(this.reverseMap.get(subProperty.getPredIri()),this.getFrameChilds(newsources,excludeNew));
    }
    return toRet;
  }
  
  private void addTypeDef(HashMap<String,Object> toObj,String typeDef)
  {
    Object tmp = toObj.get("@type");
    if(tmp == null)
    {
      toObj.put("@type",typeDef);
    }
    else if(tmp instanceof String)
    {
      LinkedList<String> newList = new LinkedList<String>();
      newList.add((String)tmp);
      newList.add(typeDef);
      toObj.put("@type",newList);
    }
    else
    {
      ((LinkedList<String>)tmp).add(typeDef);
    }
  }
    
  public void visitProperty(Property prop) throws Exception
  {
    Object obj = prop.getPredIri();
      
    if(prop.getRange().size() == 1)
    {
      Type type = prop.getRange().iterator().next();
      if(type instanceof XSDDataType)
      {
        String xsdType = ((XSDDataType)type).getClassXSDType();
        if(!xsdType.equals("xsd:string"))
        {
          HashMap<String,Object> tmp = new LinkedHashMap<String,Object>();
          tmp.put("@id",obj);
          tmp.put("@type",xsdType
              .replaceAll("xsd:anyURI","@id")
              .replaceAll("xsd:","http://www.w3.org/2001/XMLSchema#")
              .replaceAll("long","integer"));
          obj = tmp;          
        }
      }
    }
    boolean isClassRef = prop.getRange().size() != 0;
    for(Type type : prop.getRange())
    {
      if(!(type instanceof ClazzBase))
      {
        isClassRef = false;
      }
      if(type instanceof XSDDataType && prop.getRange().size() > 1)
      {
        System.out.println("WARNING: property " + prop.getPredIri() + " has multiple simple range types: " + type.getClassTypeName() );
      }
    }
    if(isClassRef)
    {
      HashMap<String,Object> tmp = new LinkedHashMap<String,Object>();
      tmp.put("@id",obj);
      tmp.put("@type","@id");
      obj = tmp; 
    }
    //dirty hack no solution available in json-ld api
    for(int i = 0;i < 100;i++)
    {
      HashMap<String,Object> tmp = new LinkedHashMap<String,Object>();
      tmp.put("@id","http://www.w3.org/1999/02/22-rdf-syntax-ns#_" + i);
      tmp.put("@type","@id");
      context.put("_" + i,tmp);
    }
    this.addShortMap(prop.getPredIri(),obj);
  }
  
  private String getName(String iri)
  {
    return iri.replace("#","/").replaceAll("^http://.*/","");
  }

  private void addShortMap(String iri,Object value)
  {
    String name = getName(iri);
    HashSet temp = this.doubleMap.get(name);
    if(temp == null)
    {
      temp = new HashSet<String>();
      this.doubleMap.put(name,temp);
    }
    temp.add(iri);
    this.context.put(iri,value);
  }
  
  @Override
  public void visitClass(Clazz clazz) throws Exception
  {
    addShortMap(clazz.getClassIRI(),clazz.getClassIRI());
  }
   
  @Override
  public void visitEnumClass(EnumClazz clazz) throws Exception
  {
    //addShortMap(clazz.getClassIRI(),clazz.getClassIRI());

   /* for(EnumItem child : clazz.getItems().values())
    {
      addShortMap(child.getClassIRI(),child.getClassIRI());
    }*/
  }
}
