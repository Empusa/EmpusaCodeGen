package org.empusa.codegen.gen;

import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.shex.domain.Schema;
import org.empusa.codegen.*;
import org.empusa.codegen.type.ExternalRefType;
import org.empusa.codegen.type.XSDDataType;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

public class DocGen implements OntologyVisitor
{
  private OntologySet ontology;
  private CommandOptions args;
  private HashMap<String,LinkedList<String>> keepUniq = new HashMap<>();
  private Schema rootSchema;
  private CodeWriter tempMkDocs;
  private HashSet<String> skipClasses = new HashSet<>();
  
  public DocGen(OntologySet ontology,CommandOptions args)
  {
    this.skipClasses.add("rdf:List");
    this.skipClasses.add("rdf:Seq");
    this.skipClasses.add("rdf:Bag");
    this.skipClasses.add("owl:Thing");
    this.ontology = ontology;
    this.args = args;
  }
  
  public void gen() throws Exception
  {
    File folder = new File(this.args.docDir);

    if (!folder.exists())
      folder.mkdirs();

    tempMkDocs = new CodeWriter(new FileWriter(this.args.docDir + "/mkdocs_temp.yml"),"  ");
    tempMkDocs.writeln("- 'Ontology classes':");
    tempMkDocs.indent();
    for(ClazzBase clazz : this.ontology.getClasses().values())
    {
      if(clazz instanceof Clazz)
        clazz.accept(this);
    }
    tempMkDocs.deindent();
    tempMkDocs.writeln("- 'EnumeratedValues':");
    tempMkDocs.indent();
    for(ClazzBase clazz : this.ontology.getClasses().values())
    {
      if(clazz instanceof EnumClazz)
        clazz.accept(this);
    }
    tempMkDocs.deindent();
    tempMkDocs.writeln("- 'Properties':");
    tempMkDocs.indent();
    for(Property prop : this.ontology.getProperties().values())
    {
      visitProperty(prop);
    }
    tempMkDocs.deindent();
    tempMkDocs.close();
  }
  
  private void printPropToMkDocs(Property prop) throws Exception
  {
    if(prop.getChilds().size() == 0)
      this.tempMkDocs.writeln("- '" + getName(prop.getPredIri()) +  "': '" + this.getPathAndName(prop.getPredIri()) + ".md'");
    else
    {
      this.tempMkDocs.writeln("- '" + getName(prop.getPredIri()) +  "':");
      this.tempMkDocs.indent();
      this.tempMkDocs.writeln("- '': '" +  this.getPathAndName(prop.getPredIri()) + ".md'");
      for(Property child : prop.getChilds())
      {
        printPropToMkDocs(child);
      }
      this.tempMkDocs.deindent();
    }
  }
  
  public void visitProperty(Property prop) throws Exception
  {
    if(prop.getParents().size() == 0)
    {
      printPropToMkDocs(prop);
    } 
    (new File(this.args.docDir + "/docs/" + this.getPath(prop.getPredIri()) + "/")).mkdirs();
    Writer writer = new FileWriter(this.args.docDir + "/docs/" + this.getPathAndName(prop.getPredIri()) + ".md");
    
    /*
     * # TestSet a rdfs:ObjectProperty rdfs:subPropetyOf [gbol:prop1](./test)
    */

    writer.write("# " + getName(prop.getPredIri()) + " a ");
    if(prop.isObjectProperty())
      writer.write("ObjectProperty");
    else
      writer.write("DatatypeProperty");
    boolean isFirst = true;
    if(prop.getParents().size() != 0)
    {
      for(Property parent : prop.getParents())
      {
        if(isFirst)
        {
          writer.write(" rdfs:subPropertyOf ");
          isFirst = false;
        }
        else
          writer.write(", ");
        writer.write(this.getPropLink(parent));
      }      
    }
    writer.write("\n\n");    
    
    /*
    ## Domain
    
    definition: Bla bla<br>
    [gbol:Class1](./test)
    
    [gbol:Class1](./test)
    */
    writer.write("## Domain\n\n");
    for(Field field : prop.getFields())
    {
      if(field.getComment() != null && !field.getComment().trim().equals(""))
        writer.write("definition: " + field.getComment() + "<br>\n");
      writer.write(this.getClassLink(field.getOwner()) + "\n\n");
    }
    
    /*    
    ## Range
    
    xsd:Integer
    
    [gbol:Class2](./test)
    */
    writer.write("## Range\n\n");
    for(Type type : prop.getRange())
    {
      if(type instanceof ClazzBase)
        writer.write(this.getClassLink((ClazzBase)type) + "\n\n");
      else if(type instanceof XSDDataType)
        writer.write(((XSDDataType)type).getClassXSDType() + "\n\n");
      else
        writer.write(type.getClassTypeName() + "\n\n");
    }
    
    /*
    ## Annotations
    
    |||
    |-----|-----|
    | test| text<br>bla|
    | test| text|

    */
    
    this.writeAnnotations(writer,prop.getPredIri());
    writer.close();
  }
  
  
  private boolean parentHasField(Clazz clazz,String iri)
  {
    for(Clazz parent : clazz.getParents())
    {
      if(parent.getFieldByIri(iri) != null)
        return true;
      if(parentHasField(parent,iri))
        return true;
    }
    return false;
  }
  
  private String getPath(String iri)
  {
    return iri.replace("#","/").replaceAll("^http://[^/]+/","").replaceAll("^(.*)/[^/]+$","$1");
  }
  
  private String getName(String iri)
  {
    return ontology.getCon().getShortForm(iri).replaceAll("^:","");
  }
  
  private String getPathAndName(String iri)
  {
    return iri.replace("#","/").replaceAll("^http://[^/]+/","");
  }
  
  private String getClassName(ClazzBase clazz)
  {
    return this.getName(clazz.getClassIRI());
  }
  
  private String getClassRelPath(ClazzBase clazz)
  {
    return "/" + this.getPathAndName(clazz.getClassIRI());
  }
  
  private String getClassLink(ClazzBase clazz)
  {
    return "[" + getClassName(clazz) + "](" + this.getClassRelPath(clazz) + ")";
  }
  
  private String getPropLink(Property prop)
  {
    return "[" + getName(prop.getPredIri()) + "](/" + this.getPathAndName(prop.getPredIri()) + ")";
  }
  
  private void printClassToMkDocs(Clazz clazz) throws Exception
  {
    if(clazz.getChilds().size() == 0)
      this.tempMkDocs.writeln("- '" + getClassName(clazz) +  "': '" + this.getPathAndName(clazz.getClassIRI()) + ".md'");
    else
    {
      this.tempMkDocs.writeln("- '" + getClassName(clazz) +  "':");
      this.tempMkDocs.indent();
      this.tempMkDocs.writeln("- '': '" + this.getPathAndName(clazz.getClassIRI()) + ".md'");
      for(Clazz child : clazz.getChilds())
      {
        printClassToMkDocs(child);
      }
      this.tempMkDocs.deindent();
    }
  }

  private void writeAnnotations(Writer writer,String iri) throws Exception
  {
    /*
      ## Annotations
      
      |||
      |-----|-----|
      | test| text<br>bla|
      | test| text|
    */

    writer.write("## Annotations\n\n");
    boolean isFirst = true;
    
    this.ontology.getCon().add("rdfs:comment","rdf:type","owl:AnnotationProperty");
    this.ontology.getCon().save("test.ttl");
    for(ResultLine line : this.ontology.getCon().runQuery("getAllAnnotPropsOfItem.sparql",true,iri))
    {
      if(isFirst)
      {
        writer.write("|||\n");
        writer.write("|-----|-----|\n");
        isFirst = false;
      }
      //?annotProp ?val
      String propName = this.ontology.getCon().getShortForm(line.getIRI("annotProp"));
      if(propName.equals("gen:propertyDefinitions"))
        continue;
      propName = propName.replaceAll("^:","");
      String val = line.getNode("val").toString().replaceAll("\n","<br>").trim().replaceAll("@..$","");

      // URL detection and fixing
      val = val.replaceAll("http://[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]", "<a href=\"$0\">$0</a>");

      writer.write("|" + propName + "|" + val + "|\n");
    }
    writer.write("\n");
  }
  
  @Override
  public void visitClass(Clazz clazz) throws Exception
  {
    if(this.skipClasses.contains(getClassName(clazz)))
      return;
    if(clazz.getParents().size() == 0)
    {
      printClassToMkDocs(clazz);
    }
    (new File(this.args.docDir + "/docs/" + this.getPath(clazz.getClassIRI()) + "/")).mkdirs();
    Writer writer = new FileWriter(this.args.docDir + "/docs/" + this.getPathAndName(clazz.getClassIRI()) + ".md");
    
    /*
        # TestClass a owl:Class extends [gbol:Class1](./test)
    */

    writer.write("# " + this.getName(clazz.getClassIRI()) + " a owl:Class");
    boolean isFirst = true;
    if(clazz.getParents().size() != 0)
    {
      for(Clazz parent : clazz.getParents())
      {
        if(isFirst)
        {
          writer.write(" extends ");
          isFirst = false;
        }
        else
          writer.write(", ");
        writer.write(this.getClassLink(parent));
      }      
    }
    writer.write("\n\n");
       
    /*
         ## Subclasses
        
        [gbol:Class1](./test)
        
        [gbol:Class1](./test)
    */
    
    writer.write("## Subclasses\n\n");
    for(Clazz child : clazz.getChilds())
    {
       writer.write(this.getClassLink(child) + "\n\n");
    }    
    
    //Annotations
    this.writeAnnotations(writer,clazz.getClassIRI());

    /*   
        ## Properties
        
        |property|description|cardinality|type|
        |-----|-----|-----|-----|
        | gbol:prop1| text<br>bla|1..N|xsd:String|
        | gbol:prop1| text<br>bla|1..N|[gbol:Class1](./test)|
        | test| text|

    */
    writer.write("## Properties\n\n");

    isFirst = true;
    for(Field field : clazz.getFields())
    {
      if(isFirst)
      {
        writer.write("|property|description|cardinality|type|\n");
        writer.write("|-----|-----|-----|-----|\n");
        isFirst = false;
      }
      String iri = field.getPredIRI();
      
      //if(args.skipNarrowingProperties && parentHasField(clazz,iri))
      //  continue;
      String cardinality = "0:N";
      if(field.isArray())
      {
        if(field.isOptional())
        {
          cardinality = "0:N";
        }
        else
        {
          cardinality = "1:N";
        }
      }
      else
      {
        if(field.isOptional())
        {
          cardinality = "0:1";
        }
        else
        {
          cardinality = "1:1";
        }
      }
      
      Type type = field.getType();
      String typeDef = "";
      if(type instanceof Clazz)
      {
        typeDef = this.getClassLink(((Clazz)type));
      }
      else if(type instanceof ExternalRefType)
      {
        typeDef = "IRI";
      }
      else if(type instanceof EnumClazz)
      {
        typeDef = this.getClassLink(((EnumClazz)type));
      }
      else 
      {
        typeDef = "xsd:" + type.getName();
      }
      writer.write("|" + this.getPropLink(field.getProperty()) + "|" + field.getComment().replaceAll("\n","<br>") + "|" + cardinality + "|" + typeDef + "|\n");
    }      
    writer.close();
  }
  
  private void printClassToMkDocs(EnumItem clazz) throws Exception
  {
    if(clazz.getChilds().size() == 0)
      this.tempMkDocs.writeln("- '" + getClassName(clazz) +  "': '" + this.getPathAndName(clazz.getClassIRI()) + ".md'");
    else
    {
      this.tempMkDocs.writeln("- '" + getClassName(clazz) +  "':");
      this.tempMkDocs.indent();
      this.tempMkDocs.writeln("- '': '" + this.getPathAndName(clazz.getClassIRI()) + ".md'");
      for(EnumItem child : clazz.getChilds())
      {
        printClassToMkDocs(child);
      }
      this.tempMkDocs.deindent();
    }
  }
  
  @Override
  public void visitEnumClass(EnumClazz clazz) throws Exception
  {
    this.tempMkDocs.writeln("- '" + getClassName(clazz) +  "':");
    this.tempMkDocs.indent();
    this.tempMkDocs.writeln("- '': '" + this.getPathAndName(clazz.getClassIRI()) + ".md'");
    for(EnumItem child : clazz.getItems().values())
    {
      printEnumChild(clazz,child);
    }
    this.tempMkDocs.deindent();
    
    (new File(this.args.docDir + "/docs/" + this.getPath(clazz.getClassIRI()) + "/")).mkdirs();
    Writer writer = new FileWriter(this.args.docDir + "/docs/" + this.getPathAndName(clazz.getClassIRI()) + ".md");
    
    /*
       # TestSet a skos:Collection
    */

    writer.write("# " + this.getName(clazz.getClassIRI()) + " a skos:Collection extends owl:Class");
    writer.write("\n\n");
    
    //Annotations
    this.writeAnnotations(writer,clazz.getClassIRI());
    /*
  
      ## skos:member
  
      [gbol:Class1](./test)
    */
    
    writer.write("## skos:member\n\n");
    for(EnumItem child : clazz.getItems().values())
    {
      writer.write(this.getClassLink(child) + "\n\n");
    }
    writer.close();
  }
  
  private void printEnumChild(EnumClazz parent,EnumItem enumItem) throws Exception
  {
    if(enumItem.getParents().size() == 0)
      printClassToMkDocs(enumItem);
    
    (new File(this.args.docDir + "/docs/" + this.getPath(enumItem.getClassIRI()) + "/")).mkdirs();
    Writer writer = new FileWriter(this.args.docDir + "/docs/" + this.getPathAndName(enumItem.getClassIRI()) + ".md");
    
    /*
      # TestSetItem a skos:Concept extends [gbol:Class1](./test),
    */

    writer.write("# " + this.getName(enumItem.getClassIRI()) + " a skos:Concept, " + this.getClassLink(parent));
    boolean isFirst = true;
    if(enumItem.getParents().size() != 0)
    {
      for(EnumItem parentClass : enumItem.getParents())
      {
        if(isFirst)
        {
          writer.write(" extends ");
          isFirst = false;
        }
        else
          writer.write(", ");
        writer.write(this.getClassLink(parentClass));
      }      
    }
    writer.write("\n\n");
    
    /*
      # TestSetItem a skos:Concept extends [gbol:Class1](./test),

      ## Subclasses
      
      [gbol:Class1](./test)
      
      [gbol:Class1](./test)
      
    */
    
    writer.write("## Subclasses\n\n");
    for(EnumItem child : enumItem.getChilds())
    {
      writer.write(this.getClassLink(child) + "\n\n");
    }
    
    //Annotations
    this.writeAnnotations(writer,enumItem.getClassIRI());
    writer.close();
  }
}
