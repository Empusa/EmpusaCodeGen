package org.empusa.codegen.gen;

import java.io.FileWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import org.empusa.codegen.Clazz;
import org.empusa.codegen.ClazzBase;
import org.empusa.codegen.CommandOptions;
import org.empusa.codegen.EnumClazz;
import org.empusa.codegen.Field;
import org.empusa.codegen.OntologySet;
import org.empusa.codegen.OntologyVisitor;
import org.empusa.codegen.Type;
import org.empusa.codegen.type.ExternalRefType;

import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.shex.domain.Annotation;
import nl.wur.ssb.shex.domain.EachOf;
import nl.wur.ssb.shex.domain.NodeConstraint;
import nl.wur.ssb.shex.domain.NodeKind;
import nl.wur.ssb.shex.domain.Schema;
import nl.wur.ssb.shex.domain.Shape;
import nl.wur.ssb.shex.domain.ShapeAnd;
import nl.wur.ssb.shex.domain.ShapeOr;
import nl.wur.ssb.shex.domain.TripleConstraint;
import nl.wur.ssb.shex.domain.objectValue;
import nl.wur.ssb.shex.domain.shapeExpr;
import nl.wur.ssb.shex.writer.ShExCWriter;

public class ShexGen implements OntologyVisitor
{
  private OntologySet ontology;
  private CommandOptions args;
  private Domain domain;
  private HashMap<String,LinkedList<String>> keepUniq = new HashMap<String,LinkedList<String>>(); 
  private HashSet<Clazz> done = new HashSet<Clazz>();
  private Schema rootSchema;
  
  public ShexGen(OntologySet ontology,CommandOptions args)
  {
    this.ontology = ontology;
    this.args = args;
  }
  
  public void gen() throws Exception
  {
    RDFSimpleCon con = new RDFSimpleCon("");
    con.addAllPrefixes(this.ontology.getCon());  
    this.domain = new Domain(con);
    rootSchema = domain.make(Schema.class,"http://" + this.ontology.getCodeProjectGroup() + "/");
    for(ClazzBase clazz : this.ontology.getClasses().values())
    {
      clazz.accept(this);
    }
    if(args.shexROutFile != null)
      con.save(args.shexROutFile);
    if(args.shexCOutFile != null)
    {
      Writer writer = new FileWriter(args.shexCOutFile);
      ShExCWriter shexWriter = new ShExCWriter(writer,this.domain);
      shexWriter.write();
      shexWriter.close();
    }
    con.close();
  }
  
  private boolean parentHasField(Clazz clazz,String iri)
  {
    for(Clazz parent : clazz.getParents())
    {
      if(parent.getFieldByIri(iri) != null)
        return true;
      if(parentHasField(parent,iri))
        return true;
    }
    return false;
  }
  
  public shapeExpr makeClass(Clazz clazz,boolean needParent) throws Exception
  {
    if(done.contains(clazz))
    {
      if(clazz.getChilds().size() != 0)
      {
        if(needParent)
          return this.domain.make(shapeExpr.class,clazz.getClassIRI());
        else
          return this.domain.make(Shape.class,clazz.getClassIRI() + "/ShapeChild");
      }
      else
        return this.domain.make(Shape.class,clazz.getClassIRI());          
    }
    done.add(clazz);
    Shape shape = null; 
    shapeExpr parentShape = null;    
    if(clazz.getChilds().size() != 0)
    {
      ShapeOr shapeOr = null;
      if(clazz.getFields().size() != 0)
        shapeOr = this.domain.make(ShapeOr.class,clazz.getClassIRI() + "/ShapeOr");
      else
        shapeOr = this.domain.make(ShapeOr.class,clazz.getClassIRI());
      
      shapeOr.setIsNamed(false);
      Shape isAShape = this.domain.make(Shape.class,clazz.getClassIRI() + "/IsAShape");
      isAShape.setIsNamed(false);
      if(!clazz.isVirtual())
      {
        EachOf masterSetExpr = this.domain.make(EachOf.class,clazz.getClassIRI() + "/tripleExprIsA");
        masterSetExpr.setMax(1);
        masterSetExpr.setMin(1);
        masterSetExpr.addExpressions(makeIsAConstraint(clazz));
        isAShape.setExpression(masterSetExpr);
        shapeOr.addShapeExprs(isAShape);
      }
      
      for(Clazz child : clazz.getChilds())
      {
        shapeOr.addShapeExprs(this.makeClass(child,true));
      }
      if(clazz.getFields().size() != 0)
      {
        ShapeAnd shapeGroup = this.domain.make(ShapeAnd.class,clazz.getClassIRI()); 
        shapeGroup.setIsNamed(true);
        parentShape = shapeGroup;
        shape = this.domain.make(Shape.class,clazz.getClassIRI() + "/ShapeChild");
        shape.setIsNamed(false);
        shapeGroup.addShapeExprs(shape);
        shapeGroup.addShapeExprs(shapeOr);
      }
      else
      {
        parentShape = shapeOr;
        parentShape.setIsNamed(true);
      }
      this.rootSchema.addShapes(parentShape);
    }
    else
    {
      parentShape = shape = this.domain.make(Shape.class,clazz.getClassIRI());
      shape.setIsNamed(true);
      this.rootSchema.addShapes(shape);
    }
    return needParent ? parentShape : shape;
  }
   
  private TripleConstraint makeIsAConstraint(Clazz clazz)
  {
    objectValue val = this.domain.make(objectValue.class,clazz.getClassIRI() + "/isAVal");
    val.setValue(clazz.getClassIRI());
    NodeConstraint nodeConstraint = this.domain.make(NodeConstraint.class,clazz.getClassIRI() + "/contraintisA");
    nodeConstraint.setNodeKind(NodeKind.IRI);
    nodeConstraint.addValues(val);
    TripleConstraint constraint = this.domain.make(TripleConstraint.class,clazz.getClassIRI() + "/isA");
    constraint.setPredicate("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
    constraint.setMax(1);
    constraint.setMin(1);
    constraint.setValueExpr(nodeConstraint);
    return constraint;
  }
  
  @Override
  public void visitClass(Clazz clazz) throws Exception
  {
    Shape shape = (Shape)makeClass(clazz,false);
 
    EachOf masterSetExpr = this.domain.make(EachOf.class,clazz.getClassIRI() + "/tripleExpr");
    masterSetExpr.setMax(1);
    masterSetExpr.setMin(1);
    boolean ok = false;
    if(clazz.getChilds().size() == 0)
    {
      masterSetExpr.addExpressions(makeIsAConstraint(clazz));
      ok = true;
    }
    int count = 0;
    for(Field field : clazz.getFields())
    {
      ok = true;
      String iri = field.getPredIRI();
      TripleConstraint constraint = this.domain.make(TripleConstraint.class,iri);
      constraint.setPredicate(iri);
      if(constraint.getAllAnnotation().size() == 0)
      {
        for(ResultLine line : this.ontology.getCon().runQuery("getAllProps.sparql",true,field.getPredIRI()))
        {
          Annotation annotation = domain.make(Annotation.class,null);
          annotation.setPredicate(line.getIRI("pred"));
          annotation.setObject(line.getNode("obj").toString());
          constraint.addAnnotation(annotation);
        } 
        if(field.getComment() != null && !field.getComment().trim().equals(""))
        {
          Annotation annotation = domain.make(Annotation.class,null);
          annotation.setPredicate("skos:definition");
          annotation.setObject(field.getComment().replaceAll("\\\"","\\\\\""));
          constraint.addAnnotation(annotation);
        }
      }

      //if(args.skipNarrowingProperties && parentHasField(clazz,iri))
      //  continue;
      if(field.isArray())
      {
        if(field.isOptional())
        {
          constraint.setMax(-1);
          constraint.setMin(0);
        }
        else
        {
          constraint.setMax(-1);
          constraint.setMin(1);
        }
      }
      else
      {
        if(field.isOptional())
        {
          constraint.setMax(1);
          constraint.setMin(0);
        }
        else
        {
          constraint.setMax(1);
          constraint.setMin(1); 
        }
      }
      
      Type type = field.getType();
      shapeExpr targetType = null;
      if(type instanceof Clazz)
      {
        targetType = makeClass(clazz,true);
        targetType.setIsNamed(true);
        this.rootSchema.addShapes(targetType);
      }
      else if(type instanceof ExternalRefType)
      {
        NodeConstraint nodeConstraint = this.domain.make(NodeConstraint.class,clazz.getClassIRI() + "/contraint" + (count++));
        nodeConstraint.setIsNamed(false);
        nodeConstraint.setNodeKind(NodeKind.IRI);
        targetType = nodeConstraint;
      }
      else if(type instanceof EnumClazz)
      {
        NodeConstraint nodeConstraint = this.domain.make(NodeConstraint.class,clazz.getClassIRI() + "/contraint" + (count++));
        nodeConstraint.setIsNamed(false);
        nodeConstraint.setNodeKind(NodeKind.IRI);
        EnumClazz enumClazz = (EnumClazz)type;
        for(String enumItem : enumClazz.getItems().keySet())
        {
          objectValue val = this.domain.make(objectValue.class,enumItem);
          val.setValue(enumItem);
          nodeConstraint.addValues(val);
        }
        targetType = nodeConstraint;
      }
      else 
      {
        NodeConstraint nodeConstraint = this.domain.make(NodeConstraint.class,clazz.getClassIRI() + "/contraint" + (count++));
        nodeConstraint.setIsNamed(false);
        nodeConstraint.setNodeKind(NodeKind.Literal);
        nodeConstraint.setDatatype("xsd:" + type.getName());
        targetType = nodeConstraint;
      }
      constraint.setValueExpr(targetType);
      masterSetExpr.addExpressions(constraint);
    }  
    if(ok)
    {
      shape.setExpression(masterSetExpr);
      //Add the annotations
      for(ResultLine line : this.ontology.getCon().runQuery("getAllProps.sparql",true,clazz.getClassIRI()))
      {
        Annotation annotation = domain.make(Annotation.class,null);
        annotation.setPredicate(line.getIRI("pred"));
        annotation.setObject(line.getNode("obj").toString());
        shape.addAnnotation(annotation);
      }  
    }
  }
  
  @Override
  public void visitEnumClass(EnumClazz clazz) throws Exception
  {
    //Do nothing not needed    
  }
}
