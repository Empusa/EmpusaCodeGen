package org.empusa.codegen.gen.owl;

import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.shex.domain.NodeConstraint;
import nl.wur.ssb.shex.domain.NodeKind;
import nl.wur.ssb.shex.domain.TripleConstraint;
import nl.wur.ssb.shex.domain.objectValue;
import org.apache.jena.rdf.model.RDFNode;
import org.empusa.codegen.*;
import org.empusa.codegen.type.XSDDataType;
import org.w3.owl.domain.AnnotationProperty;
import org.w3.owl.domain.DatatypeProperty;
import org.w3.owl.domain.ObjectProperty;
import org.w3.owl.domain.Restriction;

import java.util.HashMap;
import java.util.LinkedList;

public class OwlGen implements OntologyVisitor
{
  private OntologySet ontology;
  private CommandOptions args;
  private Domain domain;
  private Domain inDomain;
  private HashMap<Clazz,org.w3.owl.domain.Class> classes = new HashMap<>();
  private HashMap<String,PropertyHolder> properties = new HashMap<>();
  private LinkedList<AnnotationProperty> annotProps = new LinkedList<>();
  
  public OwlGen(OntologySet ontology,CommandOptions args)
  {
    this.ontology = ontology;
    this.args = args;
  }
  
  public void gen() throws Exception
  {
    RDFSimpleCon con = new RDFSimpleCon("");
    con.addAllPrefixes(this.ontology.getCon());  
    this.domain = new Domain(con);
    this.inDomain = new Domain(this.ontology.getCon());
    con.addAll(this.ontology.getCon());
    con.runUpdateQuery("cleanUnusedProps1.sparql");
   /* for(ResultLine line : this.inDomain.getRDFSimpleCon().runQuery("getAllAnnotProps.sparql",false))
    {
      annotProps.add(this.inDomain.make(AnnotationProperty.class,line.getIRI("annotProp")));
    }
    for(AnnotationProperty annotProp : annotProps)
    {
      AnnotationProperty newProp = this.domain.make(AnnotationProperty.class,annotProp.getIRI());
      for(AnnotationProperty parent : annotProp.getAllSubPropertyOf())
      {
        newProp.addSubPropertyOf(arg0);
      }
    }*/
  
    //Ontology ontology = domain.make(Ontology.class,this.ontology.getPrefix(":"));
    for(ClazzBase clazz : this.ontology.getClasses().values()) {
      clazz.accept(this);
    }
    
     buildProperties();

    // Fix exactMatch as IRI not as string
    con.runUpdateQuery("fixExactMatches.sparql");
    //
    con.runUpdateQuery("remUnUsed1.sparql");
    con.runUpdateQuery("remUnUsed1.sparql");
    con.runUpdateQuery("remUnUsed1.sparql");
    con.runUpdateQuery("remUnUsed2.sparql");
    con.runUpdateQuery("clearEnumsets.sparql");
    //Do not remove any property that referenced by equivalant or subpropertyof predicates
    con.runUpdateQuery("cleanUnusedProps2.sparql");
    //comment out this line to see which properties are removed, because they are unused
    con.runUpdateQuery("cleanUnusedProps3.sparql");
    
    con.save(args.owlOutFile);
    con.close();
  }

  private void buildProperties()
  {
    for (PropertyHolder prop : this.properties.values())
    {
      if(prop.comment != null)
        prop.property.setDefinition(prop.getComment());
      if (prop.source.size() == 1)
      {
        prop.property.setDomain(this.domain.make(org.w3.owl.domain.Class.class,prop.source.getFirst().getClassIRI()));
      }
      else if (prop.source.size() > 1)
      {
        //skip domain for rdfs:label bypass bug in protege
        if(!domain.getRDFSimpleCon().expand(prop.property.getResource().getURI()).equals("http://www.w3.org/2000/01/rdf-schema#label"))
        {
          org.w3.owl.domain.Class domainUnion = this.domain.make(org.w3.owl.domain.Class.class,null);
          for (Type item : prop.source)
          {
            ClazzBase base = (ClazzBase) item;
            domainUnion.addUnionOf(this.domain.make(org.w3.owl.domain.Class.class,base.getClassIRI()));
          }
          prop.property.setDomain(domainUnion);
        }
      }
      if (prop.property instanceof ObjectProperty)
      {
        if (prop.targetObject.size() == 1)
        {
          ((ObjectProperty) prop.property).setRange(this.domain.make(org.w3.owl.domain.Class.class,prop.targetObject.iterator().next().getClassIRI()));
        }
        else if (prop.targetObject.size() > 1)
        {
          org.w3.owl.domain.Class union = this.domain.make(org.w3.owl.domain.Class.class,null);
          for (ClazzBase item : prop.targetObject)
          {
            union.addUnionOf(this.domain.make(org.w3.owl.domain.Class.class,item.getClassIRI()));
          }
          ((ObjectProperty) prop.property).setRange(union);
        }
      }
      else
      {
        if (prop.targetDataTypes.size() == 1)
        {
          ((DatatypeProperty) prop.property)
              .setRange(this.domain.make(org.w3.rdfschema.domain.Datatype.class,prop.targetDataTypes.iterator().next().getClassXSDType()));
        }
        else if (prop.targetDataTypes.size() > 1)
        {
          org.w3.rdfschema.domain.Datatype union = this.domain.make(org.w3.rdfschema.domain.Datatype.class,null);
          for (XSDDataType item : prop.targetDataTypes)
          {
            union.addUnionOf(this.domain.make(org.w3.rdfschema.domain.Datatype.class,item.getClassXSDType()));
          }
          ((DatatypeProperty) prop.property).setRange(union);
        }
      }
    }
  }
  
  private boolean parentHasField(Clazz clazz,String iri)
  {
    for(Clazz parent : clazz.getParents())
    {
      if(parent.getFieldByIri(iri) != null)
        return true;
      if(parentHasField(parent,iri))
        return true;
    }
    return false;
  }
  
  public org.w3.owl.domain.Class makeClass(Clazz clazz) throws Exception
  {
    org.w3.owl.domain.Class genClazz = classes.get(clazz);
    if(genClazz == null)
    {
      genClazz = this.domain.make(org.w3.owl.domain.Class.class,clazz.getClassIRI());
      classes.put(clazz,genClazz);        
      for(Clazz parent : clazz.getParents())
      {
        genClazz.addSubClassOf(this.makeClass(parent));
      }
    }
    return genClazz;
  }
   
  private TripleConstraint makeIsAConstraint(Clazz clazz)
  {
    objectValue val = this.domain.make(objectValue.class,clazz.getClassIRI() + "/isAVal");
    val.setValue(clazz.getClassIRI());
    NodeConstraint nodeConstraint = this.domain.make(NodeConstraint.class,clazz.getClassIRI() + "/contraintisA");
    nodeConstraint.setNodeKind(NodeKind.IRI);
    nodeConstraint.addValues(val);
    TripleConstraint constraint = this.domain.make(TripleConstraint.class,clazz.getClassIRI() + "/isA");
    constraint.setPredicate("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
    constraint.setMax(1);
    constraint.setMin(1);
    constraint.setValueExpr(nodeConstraint);
    return constraint;
  }
  
  private PropertyHolder getMakeProp(Field field,boolean isObjectProperty)
  {
    String iri = field.getPredIRI();
    PropertyHolder toRet = this.properties.get(iri);
    if(toRet == null)
    {
      //Fix issue with properties being defined as object property and annotation propererty at the same time
      for(RDFNode node : this.domain.getRDFSimpleCon().getObjects(iri,"rdf:type").toList())
      {
        this.domain.getRDFSimpleCon().rem(iri,"rdf:type",node);
      }
      if(isObjectProperty)
        toRet = new PropertyHolder(this.domain.make(ObjectProperty.class,iri));
      else
        toRet = new PropertyHolder(this.domain.make(DatatypeProperty.class,iri));
      this.properties.put(iri,toRet);
    }
    else
    {
      if((isObjectProperty && toRet.property instanceof DatatypeProperty) ||
          (!isObjectProperty && toRet.property instanceof ObjectProperty))
      {
        System.out.println("WARNING: owl cannot model property as being both Object and Datatype property, therefor skipping property: " + iri);
        return null;
      }
    }
    toRet.addInfoFromField(field);
    return toRet;        
  }
  
  @Override
  public void visitClass(Clazz clazz) throws Exception
  {
    org.w3.owl.domain.Class genClazz = this.domain.make(org.w3.owl.domain.Class.class,clazz.getClassIRI());//makeClass(clazz);

    for(Field field : clazz.getFields())
    {
      org.w3.owl.domain.Class interSection = this.domain.make(org.w3.owl.domain.Class.class,null);
      Restriction onlyRestriction = this.domain.make(Restriction.class,null);
      Restriction cardinalityRestriction = this.domain.make(Restriction.class,null);

      Type type = field.getType();
      boolean isObjectProperty = true;
      if(type instanceof Clazz)
      {
        org.w3.owl.domain.Class expr = this.domain.make(org.w3.owl.domain.Class.class,((Clazz)type).getClassIRI());
        cardinalityRestriction.setOnClass(expr);
        onlyRestriction.setAllValuesFrom(expr);
      }
      else if(type instanceof EnumClazz)
      {
        org.w3.owl.domain.Class expr = this.domain.make(org.w3.owl.domain.Class.class,((EnumClazz)type).getClassIRI());
        cardinalityRestriction.setOnClass(expr);
        onlyRestriction.setAllValuesFrom(expr);
        /*
         *         EnumClazz enumClazz = (EnumClazz)type;
        for(String enumItem : enumClazz.getItems().keySet())
        {
          objectValue val = this.domain.make(objectValue.class,enumItem);
          val.setValue(enumItem);
          nodeConstraint.addValues(val);
        }
        targetType = nodeConstraint;
         */
      }
      else if(type instanceof XSDDataType)
      {
        isObjectProperty = false;
        org.w3.rdfschema.domain.Datatype expr = this.domain.make(org.w3.rdfschema.domain.Datatype.class,((XSDDataType)type).getClassXSDType());
        cardinalityRestriction.setOnDataRange(expr);
        onlyRestriction.setAllValuesFrom(expr);
      }
      
      PropertyHolder prop = getMakeProp(field,isObjectProperty);
      if(prop == null)
        continue;
      cardinalityRestriction.setOnProperty(prop.property);
      onlyRestriction.setOnProperty(prop.property);
      prop.source.add(clazz);
      if(isObjectProperty)
        prop.targetObject.add((ClazzBase)type);
      else
        prop.targetDataTypes.add((XSDDataType)type);
               
      //if(args.skipNarrowingProperties && parentHasField(clazz,iri))
      //  continue;
      boolean addCardinality = true;
      if(field.isArray())
      {
        if(field.isOptional())
        {
          addCardinality = false;
        }
        else
        {
          cardinalityRestriction.setMinQualifiedCardinality(1);
        }
      }
      else
      {
        if(field.isOptional())
        {
          cardinalityRestriction.setMaxQualifiedCardinality(1);
        }
        else
        {
          cardinalityRestriction.setQualifiedCardinality(1);
        }
      }
      
      if(addCardinality)
      {
        interSection.addIntersectionOf(onlyRestriction);
        interSection.addIntersectionOf(cardinalityRestriction);
        genClazz.addSubClassOf(interSection);
      }
      else
      {
        genClazz.addSubClassOf(onlyRestriction);
      }
    }  
    
    if(clazz.isVirtual())
    {
      org.w3.owl.domain.Class union = this.domain.make(org.w3.owl.domain.Class.class,null);
      if(clazz.getChilds().size() <= 1)
        throw new Exception("Virtual class: " + clazz.getName() + " must have at least 2 sub classes");
      for (Clazz childs : clazz.getChilds())
      {
        union.addUnionOf(this.domain.make(org.w3.owl.domain.Class.class,childs.getClassIRI()));
      }
      genClazz.setEquivalentClass(union);
    }
  }
  
  @Override
  public void visitEnumClass(EnumClazz clazz) throws Exception
  {
    org.w3.owl.domain.Class root = this.domain.make(org.w3.owl.domain.Class.class,clazz.getClassIRI());
    org.w3.owl.domain.Class owlClass = this.domain.make(org.w3.owl.domain.Class.class,"owl:Class");
    root.addSubClassOf(owlClass);
    for(EnumItem item : clazz.getItems().values())
    {
      //Use manual adding, dynamic class creation not supported by empusas
      this.domain.getRDFSimpleCon().add(item.getClassIRI(),"rdf:type",root.getResource());
     // for(EnumItem parent : item.getParents())
     // {
      //  this.domain.getRDFSimpleCon().add(item.getClassIRI(),"rdfs:subClassOf",parent.getClassIRI());
      //}
    }
    
    
    //Do nothing not needed    
  }
}
