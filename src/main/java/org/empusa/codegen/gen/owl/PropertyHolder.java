package org.empusa.codegen.gen.owl;

import java.util.HashSet;
import java.util.LinkedList;

import org.empusa.codegen.Clazz;
import org.empusa.codegen.ClazzBase;
import org.empusa.codegen.Field;
import org.empusa.codegen.type.XSDDataType;
import org.w3.owl.domain.Property;

public class PropertyHolder
{
  Property property;
  LinkedList<Clazz> source = new LinkedList<Clazz>();
  HashSet<ClazzBase> targetObject = new HashSet<ClazzBase>();
  HashSet<XSDDataType> targetDataTypes = new HashSet<XSDDataType>();
  String comment = null;
  LinkedList<Field> fields = new LinkedList<Field>();
  
  public PropertyHolder(Property property)
  {
    this.property = property; 
  }
  
  public void addInfoFromField(Field field)
  {
    fields.add(field);
    if(field.getComment() == null || field.getComment().trim().equals(""))
      return;
    if(comment == null)
      comment = field.getComment();
    else if(!comment.trim().equals(field.getComment().trim()))
    {
      System.out.println("WARNING: comments for field: " + field.getName() + " mismatch");
      System.out.println("-- " + comment);
      System.out.println("-- " + field.getComment());
      comment = "<multi>";
    }
  }
  
  public String getComment()
  {
    String res = comment;
    if(res == "<multi>")
    {
      res = "Field used on multiple object: \n";
      for(Field field : fields)
      {
        res += "* " + field.getOwner().getName() + " -- " + field.getComment().trim() + "\n";
      }
    }
    return res;
  }
}
