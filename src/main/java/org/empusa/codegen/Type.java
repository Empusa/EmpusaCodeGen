package org.empusa.codegen;

import com.squareup.javapoet.ClassName;

public abstract class Type
{
  public abstract String getName();
  public abstract ClassName getClassType();
  public abstract String getClassTypeName(); 
  public abstract boolean needExpectedType();
  public abstract boolean matches(Type other);
}
