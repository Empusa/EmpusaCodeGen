package org.empusa.codegen;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

import java.util.List;

@Parameters()
public class CommandOptions {
    @Parameter(names = "--help", help = true)
    private Boolean help;

    @Parameter(names = {"-i", "-input"}, description = "The ontology file and when available am additional file with metadata about the ontology", required = true, variableArity = true)
    public List<String> inOwlFile;

    @Parameter(names = {"-o", "-output"}, description = "The directory into which the API should be generated")
    public String outProjectDir;

    @Parameter(names = {"-r", "-Routput"}, description = "The directory into which the R project should be generated")
    public String outRProjectDir;

    @Parameter(names = {"-eb", "-excludeBaseFiles"}, description = "Do not overwrite the base project and build files")
    public boolean excludeBaseFiles = false;

    @Parameter(names = {"-rg", "-RDF2Graph"}, description = "File to write an RDF2Graph file")
    public String RDF2GraphOutFile;

    @Parameter(names = {"-sR", "-ShExR"}, description = "Generate ShExR file")
    public String shexROutFile;

    @Parameter(names = {"-sC", "-ShExC"}, description = "Generate ShExC file")
    public String shexCOutFile;

    @Parameter(names = {"-owl"}, description = "Generate an official OWL file")
    public String owlOutFile;

    @Parameter(names = {"-jsonld"}, description = "Generate a json framing file")
    public String jsonFrameOutFile;

    @Parameter(names = {"-doc"}, description = "Generate a documentation (markdown) page")
    public String docDir;

    @Parameter(names = {"-sNP", "-skipNarrowingProperties"}, description = "RDF2Graph export skip property already defined in parent class")
    public boolean skipNarrowingProperties = false;

    public CommandOptions(String args[]) {
        try {
            new JCommander(this, args);
            if (this.help != null) {
                new JCommander(this).usage();
                System.exit(0);
            }
        } catch (ParameterException pe) {
            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.exit(0);
        }
    }
}
