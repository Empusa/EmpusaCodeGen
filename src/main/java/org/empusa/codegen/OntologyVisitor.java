package org.empusa.codegen;

public interface OntologyVisitor
{
  public void visitClass(Clazz clazz) throws Exception;
  public void visitEnumClass(EnumClazz clazz) throws Exception;
}
