package org.empusa.codegen;

import java.io.File;

import org.apache.commons.io.FileUtils;

public class MakeRDF2GraphOntology
{
  public static void main(String args[]) throws Exception
  {
    FileUtils.deleteDirectory(new File("./nl/wur/ssb/RDF2Graph/domain"));
    args = new String[]{"-i","RDF2Graph.ttl","-o","./","-eb"};//"-rg","AsRDF2Graph.ttl"
    Main.main(args);
  }
}
