package org.empusa.codegen;

import java.util.LinkedList;

import com.squareup.javapoet.ClassName;

public class EnumItem extends ClazzBase
{
  private LinkedList<EnumItem> parents = new LinkedList<EnumItem>();
  private LinkedList<EnumItem> childs = new LinkedList<EnumItem>();
  String name; 

  public EnumItem(Ontology ontology, String classIRI,String name)
  {
    super(ontology,classIRI);
    this.name = name; 
  }
  
  public String toString()
  {
    return "EnumItem: " + this.name;
  }

  public LinkedList<EnumItem> getParents()
  {
    return parents;
  }
  
  public void addParent(EnumItem parent)
  {
    this.parents.add(parent);
    parent.childs.add(this);
  }

  public LinkedList<EnumItem> getChilds()
  {
    return childs;
  }

  public String getName()
  {
    return name;
  }

  @Override
  public void preParse() throws Exception
  {
 
  }

  @Override
  public void postParse() throws Exception
  {

  }

  @Override
  public void accept(OntologyVisitor visitor) throws Exception
  {

  }

  @Override
  public ClassName getClassType()
  {
    return null;
  }

  @Override
  public String getClassTypeName()
  {
    return null;
  }

  @Override
  public boolean needExpectedType()
  {
    return false;
  }

  @Override
  public boolean matches(Type other)
  { 
    return false;
  }  
}


