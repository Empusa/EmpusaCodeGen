package org.empusa.codegen;

import com.squareup.javapoet.ClassName;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

import java.util.*;

public class Clazz extends ClazzBase
{
  private String name;
  private HashMap<String,Field> fields = new HashMap<>();
  private LinkedList<Clazz> parents = new LinkedList<>();
  private LinkedList<Clazz> childs = new LinkedList<>();
  private LinkedList<Field> framedFields = new LinkedList<>();
  private HashSet<String> namesUsed = new HashSet<>();

  private boolean isVirtual;
  
  public Clazz(Ontology ontology, String classIRI)
  {
    super(ontology,classIRI);
    this.ontology.addClass(classIRI,this);
    name = classIRI.replaceAll("^.*[/#]","");  
  }
  
  public Field getFieldByIri(String iri)
  {
    return this.fields.get(iri);
  }
  
  private void parseFields() throws Exception
  {
    // Get subclass of properties?
    namesUsed = new HashSet<>();

    for(ResultLine item : ontology.getOntologySet().getCon().runQuery("getParentProperties.sparql",false,classIRI)) {
      parseItems(item);
    }

    for(ResultLine item : ontology.getOntologySet().getCon().runQuery("getProperties.sparql",false,classIRI))
    {
      parseItems(item);
    }
  }

  private void parseItems(ResultLine item) throws Exception {

    String defSet = item.getLitString("propertyDef");

    ArrayList<String> chunks = new ArrayList<>();
    String chunk = "";
    //Split on ; but not the ones that are in the comment section
    for (String line : defSet.split("\n"))
    {
      line = line.trim();
      String tmp[] = line.split("#",2);
      boolean isFirst = true;
      for(String part : tmp[0].split(";"))
      {
        if(isFirst)
        {
          chunk += part;
          isFirst = false;
        }
        else
        {
          chunks.add(chunk);
          chunk = part;
        }
      }
      if(tmp[0].endsWith(";"))
      {
        chunks.add(chunk);
        chunk = "";
      }
      if(tmp.length == 2)
        chunk += "#" + tmp[1];
      chunk += "\n";
    }
    chunks.add(chunk);
    for(String def : chunks)
    {
      String comment = "";
      def = def.trim();
      for (String line : def.split("\n"))
      {
        if (line.startsWith("#"))
        {
          if (line.toLowerCase().contains("todo"))
            System.out.println("TODO: " + line);
          if (line.startsWith("#*"))
          {
            comment += line.substring(2).trim() + "\n";
          }
        }
      }

      def = def.replaceAll("\\s*#[^\n]*","").trim();
      if(def.equals(""))
        continue;

      Field field = Field.parse(ontology.getOntologySet(),def,ontology.getOntologySet().getPrefix(":"),this,comment);
      if(field != null)
      {
        fields.put(field.getPredIRI(),field);
        if(namesUsed.contains(field.name))
          throw new Exception("Error: 2 fields with the same name >" + field.name + "< in class: " + this.name);
        namesUsed.add(field.name);
        if(field.getType() instanceof ClazzBase)
        {
          this.ontology.addDependsOn(((ClazzBase)field.getType()).getOntology());
        }
      }
    }
  }

  private void parseFraming() throws Exception
  {
    for(ResultLine item : ontology.getOntologySet().getCon().runQuery("getFraming.sparql",false,classIRI))
    {
      String defSet = item.getLitString("frameDef");

      for (String line : defSet.split("\n"))
      {
        line = line.replaceAll("#.*","").trim();
        if(line.equals(""))
          continue;
        if(line.equals("ROOT"))
        {
          this.getOntologySet().addFrameRoot(this);
          continue;
        }
        
        line = line.replaceAll("\\s+"," ");
        String tmp[] = (line.split(" - ",2));
        String pred = expand(tmp[0]);

        Field field = this.fields.get(pred);
        if(field == null)
          throw new Exception("unknown framing field" + pred);
        
        if(tmp.length == 2)
        {
          String parts[] = tmp[1].split(" "); 
          for(String part : parts)
          {
            part = expand(part); 
            Clazz clazz = (Clazz)this.getOntologySet().getClass(part);
            if(clazz == null)
              throw new Exception("unknonw type to exclude: " + part);
            field.addExcludeTypeInFraming(clazz);
          }
        }
        
        this.framedFields.add(field);
      }
    }
  }
  private String expand(String iri) throws Exception
  {
    if(iri.startsWith(":"))
      iri = iri.substring(1);
    String toRet = ontology.getOntologySet().getPrefix(":") + iri;
    String tmp[] = iri.split(":");
    if(tmp.length == 2)
    {
      String fullIri = this.getOntologySet().getPrefix(tmp[0]);
      if(fullIri == null)
        throw new Exception("Prefix unknown:" + tmp[0]);
      iri = tmp[1];
      toRet = fullIri + iri;      
    }
    
    return toRet;
  }
  
  private void parseParents() throws Exception
  {
    for(ResultLine item : ontology.getOntologySet().getCon().runQuery("getSuperClass.sparql",false,classIRI))
    {
      String iri = item.getIRI("parent");
      if(iri == null)
        throw new Exception("iri =  null: " + classIRI);
      if(iri.equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#Bag") || iri.equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#Seq"))
        continue;// TODO
      //The default base class skip it
      if(iri.equals("http://www.w3.org/2002/07/owl#Thing"))
        continue;
      String parentName = iri.replaceAll("^.*[/#]","");
      Clazz parent = (Clazz)ontology.getOntologySet().getClass(iri);
      if(parent == null)
        throw new Exception("Parent class not found:" + parentName);
      this.parents.add(parent);
      parent.childs.add(this);
      this.ontology.addDependsOn(parent.getOntology());
    }
  }
  
  @Override
  public void preParse() throws Exception
  {
    parseParents();
    parseFields();
    parseFraming();
    for(Field field : this.fields.values())
      this.getOntologySet().addFieldToProperties(field);
    this.isVirtual = this.ontology.getOntologySet().getCon().containsLit(classIRI,"http://empusa.org/0.1/virtual",true);
  }
   
  public boolean isVirtual()
  {
    return isVirtual;
  }

  public void postParse() throws Exception
  {
    int count = 0;
    Clazz walker = this;
    while(walker != null)
    {
      for(Field field : walker.fields.values())
      {
        if(field.isBag || field.isSeq)
          count++;
      }
      if(walker.parents.size() != 0)
        walker = walker.parents.get(0);
      else
        walker = null;
    }
    if(count > 1)
      throw new Exception("Only bag or seq typed parameter allowod");
    //Fill in the - for the comments
    for(Field field : this.fields.values())
    {
      field.copyCommentFromParent(this);
    }
  }

  @Override
  public void accept(OntologyVisitor visitor) throws Exception
  {
    visitor.visitClass(this);    
  }

  public String getName()
  {
    return name;
  }

  public Collection<Field> getFields()
  {
    return fields.values();
  }

  public LinkedList<Clazz> getParents()
  {
    return parents;
  }
    
  public LinkedList<Clazz> getChilds()
  {
    return childs;
  }

  @Override
  public ClassName getClassType()
  {
    return ClassName.get(OWLThingImpl.getBasePackage(this.classIRI) + ".domain",this.name);
  }

  @Override
  public String getClassTypeName()
  {
    return "Ref";
  }
  
  @Override
  public boolean needExpectedType()
  {
    return true;
  }
  
  @Override
  public boolean matches(Type other)
  {
    if(other instanceof Clazz)
    {
      Clazz otherClazz = (Clazz)other;
      return this.isTypeOf(otherClazz);
    }
    return false;
  }
  
  public boolean isTypeOf(Clazz other)
  {
    if(other == this)
      return true;
    for(Clazz parent : this.getParents())
    {
      if(parent.isTypeOf(other))
        return true;
    }
    return false;
  }
  
  public HashSet<Clazz> getAllChilds()
  {
    HashSet<Clazz> toRet = new HashSet<>();
    for(Clazz child : this.childs)
    {
      toRet.addAll(child.getAllChilds());
      toRet.add(child);
    }
    return toRet;
  }

  public LinkedList<Field> getFramedFields()
  {
    return framedFields;
  }
}
