package org.empusa.codegen;

import java.util.HashMap;
import java.util.HashSet;

public class Ontology
{
  private HashMap<String,ClazzBase> classes = new HashMap<String,ClazzBase>();
  private HashSet<Ontology> dependsOn = new HashSet<Ontology>();
  private String baseIri;
  private OntologySet ontologySet;
 
  public Ontology(OntologySet ontologySet,String baseIri)
  {
    this.ontologySet = ontologySet;
    this.baseIri = baseIri;
  }
  
  public void addClass(String iri, ClazzBase clazz)
  {
    this.classes.put(iri,clazz);
    this.ontologySet.addClass(iri,clazz);
  }

  public OntologySet getOntologySet()
  {
    return ontologySet;
  }  
  
  public void addDependsOn(Ontology ontology)
  {
    if(ontology != this)
      dependsOn.add(ontology);
  }
  public boolean checkDependTree(HashSet<Ontology> collected)
  {
    for(Ontology item : this.dependsOn)
    {
      if(collected.contains(item))
        return false;
    }
    HashSet<Ontology> newSet = new HashSet<Ontology>();
    newSet.addAll(collected);
    newSet.add(this);
    for(Ontology item : this.dependsOn)
    {
      if(!item.checkDependTree(newSet))
        return false;
    }
    return true;
  }
  
  public String getTree(HashSet<Ontology> collected,String indent)
  {
    String toRet = "";
    HashSet<Ontology> newSet = new HashSet<Ontology>();
    newSet.addAll(collected);
    newSet.addAll(dependsOn);
    for(Ontology item : this.dependsOn)
    {     
      if(collected.contains(item))
        toRet += indent + "" + item.baseIri + item +" -- X\n";
      else
        toRet += indent + "" + item.baseIri + item + "\n" + item.getTree(newSet,indent + "  ");
    }
    return toRet;
  }

  public String getBaseIri()
  {
    return baseIri;
  }

  public HashMap<String, ClazzBase> getClasses()
  {
    return classes;
  }
}
