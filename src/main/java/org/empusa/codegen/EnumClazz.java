package org.empusa.codegen;

import java.util.HashMap;

import org.empusa.codegen.gen.JavaCodeGen;

import com.squareup.javapoet.ClassName;

import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;

public class EnumClazz extends ClazzBase
{
  private HashMap<String,EnumItem> items = new HashMap<String,EnumItem>();
  private String name;
  
  public HashMap<String, EnumItem> getItems()
  {
    return items;
  }

  public String getName()
  {
    return name;
  }

  public EnumClazz(Ontology ontology, String classIRI)
  {
    super(ontology,classIRI);
    this.ontology.addClass(classIRI,this);
    name = classIRI.replaceAll("^.*[/#]","");
  }

  private String filterEnumName(String name)
  {
    if(name.equals("NA"))
      return "NA_";
    return name;
  }
  
  private void parseDefs() throws Exception
  {
    for(ResultLine item : ontology.getOntologySet().getCon().runQuery("getEnumDef.sparql",false,classIRI))
    {
      //SELECT ?item ?parent
      String defIri = item.getIRI("item");
      String defName = filterEnumName(defIri.replaceAll("^.*[/#]",""));
      //readding will just remove the old one, I know this just works
      this.items.put(defIri,new EnumItem(this.ontology,defIri,defName));
    }
    for(ResultLine item : ontology.getOntologySet().getCon().runQuery("getEnumDef.sparql",false,classIRI))
    {
      //SELECT ?item ?parent
      String defIri = item.getIRI("item");
      String parentIri = item.getIRI("parent");
      if(parentIri.equals(this.classIRI))
        continue;
      this.items.get(defIri).addParent(this.items.get(parentIri));
    }
  }
  
  @Override
  public void preParse() throws Exception
  {
    parseDefs();    
  }    

  @Override
  public void postParse() throws Exception
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void accept(OntologyVisitor visitor) throws Exception
  {
    visitor.visitEnumClass(this);    
  }  
  
  @Override
  public ClassName getClassType()
  {
    return  ClassName.get(OWLThingImpl.getBasePackage(this.classIRI) + ".domain",this.name);
  }

  @Override
  public String getClassTypeName()
  {
    return "Enum";
  }
  
  @Override
  public boolean needExpectedType()
  {
    return true;
  }

  @Override
  public boolean matches(Type other)
  {
    if(other instanceof EnumClazz && ((EnumClazz)other).getClassIRI().equals(this.classIRI))
      return true;
    // TODO Auto-generated method stub
    return false;
  }
}
