package org.empusa.RDF2Graph.domain.impl;

import java.lang.Integer;
import java.lang.String;
import java.util.List;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.empusa.RDF2Graph.domain.Class;
import org.empusa.RDF2Graph.domain.ClassProperty;
import org.empusa.RDF2Graph.domain.Error;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public class ClassImpl extends TypeImpl implements Class {
  public static final String TypeIRI = "http://ssb.wur.nl/RDF2Graph/Class";

  protected ClassImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Class make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ClassImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Class.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Class.class,false);
          if(toRet == null) {
            toRet = new ClassImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Class)) {
          throw new RuntimeException("Instance of nl.wur.ssb.RDF2Graph.domain.impl.ClassImpl expected");
        }
      }
      return (Class)toRet;
    }
  }

  public void validate() {
  }

  public void remProperty(ClassProperty val) {
    this.remRef("http://ssb.wur.nl/RDF2Graph/property",val,true);
  }

  public List<? extends ClassProperty> getAllProperty() {
    return this.getRefSet("http://ssb.wur.nl/RDF2Graph/property",true,ClassProperty.class);
  }

  public void addProperty(ClassProperty val) {
    this.addRef("http://ssb.wur.nl/RDF2Graph/property",val);
  }

  public void remSubClassOf(Class val) {
    this.remRef("http://www.w3.org/2000/01/rdf-schema#subClassOf",val,true);
  }

  public List<? extends Class> getAllSubClassOf() {
    return this.getRefSet("http://www.w3.org/2000/01/rdf-schema#subClassOf",true,Class.class);
  }

  public void addSubClassOf(Class val) {
    this.addRef("http://www.w3.org/2000/01/rdf-schema#subClassOf",val);
  }

  public Error getError() {
    return this.getRef("http://ssb.wur.nl/RDF2Graph/error",true,Error.class);
  }

  public void setError(Error val) {
    this.setRef("http://ssb.wur.nl/RDF2Graph/error",val,Error.class);
  }

  public String getComment() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#comment",true);
  }

  public void setComment(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#comment",val);
  }

  public String getLabel() {
    return this.getStringLit("http://www.w3.org/2000/01/rdf-schema#label",true);
  }

  public void setLabel(String val) {
    this.setStringLit("http://www.w3.org/2000/01/rdf-schema#label",val);
  }

  public Integer getCount() {
    return this.getIntegerLit("http://ssb.wur.nl/RDF2Graph/count",true);
  }

  public void setCount(Integer val) {
    this.setIntegerLit("http://ssb.wur.nl/RDF2Graph/count",val);
  }

  public Integer getSubClassOfInstanceCount() {
    return this.getIntegerLit("http://ssb.wur.nl/RDF2Graph/subClassOfInstanceCount",true);
  }

  public void setSubClassOfInstanceCount(Integer val) {
    this.setIntegerLit("http://ssb.wur.nl/RDF2Graph/subClassOfInstanceCount",val);
  }
}
