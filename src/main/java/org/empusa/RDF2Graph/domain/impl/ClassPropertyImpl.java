package org.empusa.RDF2Graph.domain.impl;

import java.lang.String;
import java.util.List;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.empusa.RDF2Graph.domain.ClassProperty;
import org.empusa.RDF2Graph.domain.TypeLink;
import org.w3.rdfsyntaxns.domain.Property;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public class ClassPropertyImpl extends OWLThingImpl implements ClassProperty {
  public static final String TypeIRI = "http://ssb.wur.nl/RDF2Graph/ClassProperty";

  protected ClassPropertyImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ClassProperty make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ClassPropertyImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ClassProperty.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ClassProperty.class,false);
          if(toRet == null) {
            toRet = new ClassPropertyImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ClassProperty)) {
          throw new RuntimeException("Instance of nl.wur.ssb.RDF2Graph.domain.impl.ClassPropertyImpl expected");
        }
      }
      return (ClassProperty)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://ssb.wur.nl/RDF2Graph/rdfProperty");
  }

  public Property getRdfProperty() {
    return this.getRef("http://ssb.wur.nl/RDF2Graph/rdfProperty",false,Property.class);
  }

  public void setRdfProperty(Property val) {
    this.setRef("http://ssb.wur.nl/RDF2Graph/rdfProperty",val,Property.class);
  }

  public void remLinkTo(TypeLink val) {
    this.remRef("http://ssb.wur.nl/RDF2Graph/linkTo",val,true);
  }

  public List<? extends TypeLink> getAllLinkTo() {
    return this.getRefSet("http://ssb.wur.nl/RDF2Graph/linkTo",true,TypeLink.class);
  }

  public void addLinkTo(TypeLink val) {
    this.addRef("http://ssb.wur.nl/RDF2Graph/linkTo",val);
  }

  public String getNoteAsNew() {
    return this.getStringLit("http://ssb.wur.nl/RDF2Graph/noteAsNew",true);
  }

  public void setNoteAsNew(String val) {
    this.setStringLit("http://ssb.wur.nl/RDF2Graph/noteAsNew",val);
  }
}
