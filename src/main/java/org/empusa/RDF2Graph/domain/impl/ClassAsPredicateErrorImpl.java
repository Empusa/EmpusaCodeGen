package org.empusa.RDF2Graph.domain.impl;

import java.lang.String;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.empusa.RDF2Graph.domain.ClassAsPredicateError;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public class ClassAsPredicateErrorImpl extends ErrorImpl implements ClassAsPredicateError {
  public static final String TypeIRI = "http://ssb.wur.nl/RDF2Graph/ClassAsPredicateError";

  protected ClassAsPredicateErrorImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ClassAsPredicateError make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ClassAsPredicateErrorImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ClassAsPredicateError.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ClassAsPredicateError.class,false);
          if(toRet == null) {
            toRet = new ClassAsPredicateErrorImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ClassAsPredicateError)) {
          throw new RuntimeException("Instance of nl.wur.ssb.RDF2Graph.domain.impl.ClassAsPredicateErrorImpl expected");
        }
      }
      return (ClassAsPredicateError)toRet;
    }
  }

  public void validate() {
  }
}
