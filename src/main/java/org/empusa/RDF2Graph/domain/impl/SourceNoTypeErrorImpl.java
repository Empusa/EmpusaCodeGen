package org.empusa.RDF2Graph.domain.impl;

import java.lang.String;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.empusa.RDF2Graph.domain.SourceNoTypeError;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public class SourceNoTypeErrorImpl extends ErrorImpl implements SourceNoTypeError {
  public static final String TypeIRI = "http://ssb.wur.nl/RDF2Graph/SourceNoTypeError";

  protected SourceNoTypeErrorImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static SourceNoTypeError make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new SourceNoTypeErrorImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,SourceNoTypeError.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,SourceNoTypeError.class,false);
          if(toRet == null) {
            toRet = new SourceNoTypeErrorImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof SourceNoTypeError)) {
          throw new RuntimeException("Instance of nl.wur.ssb.RDF2Graph.domain.impl.SourceNoTypeErrorImpl expected");
        }
      }
      return (SourceNoTypeError)toRet;
    }
  }

  public void validate() {
  }
}
