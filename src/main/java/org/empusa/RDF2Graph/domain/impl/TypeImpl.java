package org.empusa.RDF2Graph.domain.impl;

import java.lang.String;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.empusa.RDF2Graph.domain.Type;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public class TypeImpl extends OWLThingImpl implements Type {
  public static final String TypeIRI = "http://ssb.wur.nl/RDF2Graph/Type";

  protected TypeImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Type make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new TypeImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Type.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Type.class,false);
          if(toRet == null) {
            toRet = new TypeImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Type)) {
          throw new RuntimeException("Instance of nl.wur.ssb.RDF2Graph.domain.impl.TypeImpl expected");
        }
      }
      return (Type)toRet;
    }
  }

  public void validate() {
  }
}
