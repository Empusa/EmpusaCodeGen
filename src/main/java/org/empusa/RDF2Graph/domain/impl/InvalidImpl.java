package org.empusa.RDF2Graph.domain.impl;

import java.lang.String;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.empusa.RDF2Graph.domain.Invalid;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public class InvalidImpl extends TypeImpl implements Invalid {
  public static final String TypeIRI = "http://ssb.wur.nl/RDF2Graph/Invalid";

  protected InvalidImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Invalid make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new InvalidImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Invalid.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Invalid.class,false);
          if(toRet == null) {
            toRet = new InvalidImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Invalid)) {
          throw new RuntimeException("Instance of nl.wur.ssb.RDF2Graph.domain.impl.InvalidImpl expected");
        }
      }
      return (Invalid)toRet;
    }
  }

  public void validate() {
  }
}
