package org.empusa.RDF2Graph.domain.impl;

import java.lang.String;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.empusa.RDF2Graph.domain.DestNoTypeError;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public class DestNoTypeErrorImpl extends ErrorImpl implements DestNoTypeError {
  public static final String TypeIRI = "http://ssb.wur.nl/RDF2Graph/DestNoTypeError";

  protected DestNoTypeErrorImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static DestNoTypeError make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new DestNoTypeErrorImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,DestNoTypeError.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,DestNoTypeError.class,false);
          if(toRet == null) {
            toRet = new DestNoTypeErrorImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof DestNoTypeError)) {
          throw new RuntimeException("Instance of nl.wur.ssb.RDF2Graph.domain.impl.DestNoTypeErrorImpl expected");
        }
      }
      return (DestNoTypeError)toRet;
    }
  }

  public void validate() {
  }
}
