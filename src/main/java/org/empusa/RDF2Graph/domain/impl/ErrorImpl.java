package org.empusa.RDF2Graph.domain.impl;

import java.lang.String;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.empusa.RDF2Graph.domain.Error;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public class ErrorImpl extends OWLThingImpl implements Error {
  public static final String TypeIRI = "http://ssb.wur.nl/RDF2Graph/Error";

  protected ErrorImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Error make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ErrorImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Error.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Error.class,false);
          if(toRet == null) {
            toRet = new ErrorImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Error)) {
          throw new RuntimeException("Instance of nl.wur.ssb.RDF2Graph.domain.impl.ErrorImpl expected");
        }
      }
      return (Error)toRet;
    }
  }

  public void validate() {
  }
}
