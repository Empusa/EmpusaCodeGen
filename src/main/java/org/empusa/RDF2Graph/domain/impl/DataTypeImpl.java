package org.empusa.RDF2Graph.domain.impl;

import java.lang.String;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.empusa.RDF2Graph.domain.DataType;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public class DataTypeImpl extends TypeImpl implements DataType {
  public static final String TypeIRI = "http://ssb.wur.nl/RDF2Graph/DataType";

  protected DataTypeImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static DataType make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new DataTypeImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,DataType.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,DataType.class,false);
          if(toRet == null) {
            toRet = new DataTypeImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof DataType)) {
          throw new RuntimeException("Instance of nl.wur.ssb.RDF2Graph.domain.impl.DataTypeImpl expected");
        }
      }
      return (DataType)toRet;
    }
  }

  public void validate() {
  }
}
