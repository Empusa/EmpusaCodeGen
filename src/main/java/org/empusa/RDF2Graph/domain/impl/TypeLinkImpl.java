package org.empusa.RDF2Graph.domain.impl;

import java.lang.Integer;
import java.lang.String;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.empusa.RDF2Graph.domain.Multiplicity;
import org.empusa.RDF2Graph.domain.Type;
import org.empusa.RDF2Graph.domain.TypeLink;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public class TypeLinkImpl extends OWLThingImpl implements TypeLink {
  public static final String TypeIRI = "http://ssb.wur.nl/RDF2Graph/TypeLink";

  protected TypeLinkImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static TypeLink make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new TypeLinkImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,TypeLink.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,TypeLink.class,false);
          if(toRet == null) {
            toRet = new TypeLinkImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof TypeLink)) {
          throw new RuntimeException("Instance of nl.wur.ssb.RDF2Graph.domain.impl.TypeLinkImpl expected");
        }
      }
      return (TypeLink)toRet;
    }
  }

  public void validate() {
    this.checkCardMin1("http://ssb.wur.nl/RDF2Graph/reverseMultiplicity");
    this.checkCardMin1("http://ssb.wur.nl/RDF2Graph/forwardMultiplicity");
    this.checkCardMin1("http://ssb.wur.nl/RDF2Graph/type");
  }

  public Multiplicity getReverseMultiplicity() {
    return this.getEnum("http://ssb.wur.nl/RDF2Graph/reverseMultiplicity",false,Multiplicity.class);
  }

  public void setReverseMultiplicity(Multiplicity val) {
    this.setEnum("http://ssb.wur.nl/RDF2Graph/reverseMultiplicity",val,Multiplicity.class);
  }

  public Multiplicity getForwardMultiplicity() {
    return this.getEnum("http://ssb.wur.nl/RDF2Graph/forwardMultiplicity",false,Multiplicity.class);
  }

  public void setForwardMultiplicity(Multiplicity val) {
    this.setEnum("http://ssb.wur.nl/RDF2Graph/forwardMultiplicity",val,Multiplicity.class);
  }

  public Type getType() {
    return this.getRef("http://ssb.wur.nl/RDF2Graph/type",false,Type.class);
  }

  public void setType(Type val) {
    this.setRef("http://ssb.wur.nl/RDF2Graph/type",val,Type.class);
  }

  public Integer getCount() {
    return this.getIntegerLit("http://ssb.wur.nl/RDF2Graph/count",true);
  }

  public void setCount(Integer val) {
    this.setIntegerLit("http://ssb.wur.nl/RDF2Graph/count",val);
  }
}
