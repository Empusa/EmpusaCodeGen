package org.empusa.RDF2Graph.domain.impl;

import java.lang.String;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.empusa.RDF2Graph.domain.ExternalReference;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public class ExternalReferenceImpl extends TypeImpl implements ExternalReference {
  public static final String TypeIRI = "http://ssb.wur.nl/RDF2Graph/ExternalReference";

  protected ExternalReferenceImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ExternalReference make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ExternalReferenceImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ExternalReference.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ExternalReference.class,false);
          if(toRet == null) {
            toRet = new ExternalReferenceImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ExternalReference)) {
          throw new RuntimeException("Instance of nl.wur.ssb.RDF2Graph.domain.impl.ExternalReferenceImpl expected");
        }
      }
      return (ExternalReference)toRet;
    }
  }

  public void validate() {
  }
}
