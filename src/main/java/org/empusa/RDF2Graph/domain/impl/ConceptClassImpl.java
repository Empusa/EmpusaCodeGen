package org.empusa.RDF2Graph.domain.impl;

import java.lang.String;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.empusa.RDF2Graph.domain.ConceptClass;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public class ConceptClassImpl extends ClassImpl implements ConceptClass {
  public static final String TypeIRI = "http://ssb.wur.nl/RDF2Graph/ConceptClass";

  protected ConceptClassImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static ConceptClass make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new ConceptClassImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,ConceptClass.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,ConceptClass.class,false);
          if(toRet == null) {
            toRet = new ConceptClassImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof ConceptClass)) {
          throw new RuntimeException("Instance of nl.wur.ssb.RDF2Graph.domain.impl.ConceptClassImpl expected");
        }
      }
      return (ConceptClass)toRet;
    }
  }

  public void validate() {
  }
}
