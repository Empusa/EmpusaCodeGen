package org.empusa.RDF2Graph.domain;

import java.lang.Integer;
import java.lang.String;
import java.util.List;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public interface Class extends Type {
  void remProperty(ClassProperty val);

  List<? extends ClassProperty> getAllProperty();

  void addProperty(ClassProperty val);

  void remSubClassOf(Class val);

  List<? extends Class> getAllSubClassOf();

  void addSubClassOf(Class val);

  Error getError();

  void setError(Error val);

  String getComment();

  void setComment(String val);

  String getLabel();

  void setLabel(String val);

  Integer getCount();

  void setCount(Integer val);

  Integer getSubClassOfInstanceCount();

  void setSubClassOfInstanceCount(Integer val);
}
