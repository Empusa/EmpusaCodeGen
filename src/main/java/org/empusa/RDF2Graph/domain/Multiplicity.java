package org.empusa.RDF2Graph.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.EnumClass;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public enum Multiplicity implements EnumClass {
  x_1("http://ssb.wur.nl/RDF2Graph/x_1",new Multiplicity[]{}),

  Exactly_one("http://open-services.net/ns/core#Exactly-one",new Multiplicity[]{}),

  none("http://ssb.wur.nl/RDF2Graph/none",new Multiplicity[]{}),

  One_or_many("http://open-services.net/ns/core#One-or-many",new Multiplicity[]{}),

  Zero_or_many("http://open-services.net/ns/core#Zero-or-many",new Multiplicity[]{}),

  x_n("http://ssb.wur.nl/RDF2Graph/x_n",new Multiplicity[]{}),

  Zero_or_one("http://open-services.net/ns/core#Zero-or-one",new Multiplicity[]{});

  private Multiplicity[] parents;

  private String iri;

  private Multiplicity(String iri, Multiplicity[] parents) {
    this.iri = iri;
    this.parents = parents;
  }

  public EnumClass[] getParents() {
    return parents;
  }

  public String getIRI() {
    return this.iri;
  }

  public static Multiplicity make(String iri) {
    for(Multiplicity item : Multiplicity.values()) {
      if(item.iri.equals(iri)) {
        return item;
      }
    }
    throw new RuntimeException("Enum value: " + iri + " not found for enum type: EnumExample");
  }
}
