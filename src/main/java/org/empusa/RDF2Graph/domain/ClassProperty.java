package org.empusa.RDF2Graph.domain;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;
import org.w3.rdfsyntaxns.domain.Property;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public interface ClassProperty extends OWLThing {
  Property getRdfProperty();

  void setRdfProperty(Property val);

  void remLinkTo(TypeLink val);

  List<? extends TypeLink> getAllLinkTo();

  void addLinkTo(TypeLink val);

  String getNoteAsNew();

  void setNoteAsNew(String val);
}
