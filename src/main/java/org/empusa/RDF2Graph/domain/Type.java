package org.empusa.RDF2Graph.domain;

import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public interface Type extends OWLThing {
}
