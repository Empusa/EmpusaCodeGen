package org.empusa.RDF2Graph.domain;

import java.lang.Integer;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://ssb.wur.nl/RDF2Graph/ ontology
 */
public interface TypeLink extends OWLThing {
  Multiplicity getReverseMultiplicity();

  void setReverseMultiplicity(Multiplicity val);

  Multiplicity getForwardMultiplicity();

  void setForwardMultiplicity(Multiplicity val);

  Type getType();

  void setType(Type val);

  Integer getCount();

  void setCount(Integer val);
}
