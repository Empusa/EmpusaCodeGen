package org.w3.rdfsyntaxns.domain.impl;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.w3.rdfsyntaxns.domain.Property;

/**
 * Code generated from http://www.w3.org/1999/02/22-rdf-syntax-ns# ontology
 */
public class PropertyImpl extends OWLThingImpl implements Property {
  public static final String TypeIRI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#Property";

  protected PropertyImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Property make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new PropertyImpl(domain,resource);;
      }
      else {
        toRet = domain.getObject(resource,Property.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Property.class,false);
          if(toRet == null) {
            toRet = new PropertyImpl(domain,resource);;
          }
        }
        else if(!(toRet instanceof Property)) {
          throw new RuntimeException("Instance of org.w3.rdfsyntaxns.domain.impl.PropertyImpl expected");
        }
      }
      return (Property)toRet;
    }
  }

  public void validate() {
  }
}
