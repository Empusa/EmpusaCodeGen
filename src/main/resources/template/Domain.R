.onLoad <- function(libname, pkgname) {
  .jinit(parameters="-Xmx4000m")
  .jpackage(pkgname, lib.loc=libname)
}

.onAttach <- function(libname, pkgname) {
  %3$s
}

TypeMap <- new.env(hash=TRUE)

DomainInner<- setRefClass(
  "DomainInner",
  field = list(
      env = "ANY",
      domain = "ANY"
  ),
  methods = list(
    initialize = function(domainIn)
    {
       env <<- new.env(hash=TRUE)
       domain <<- domainIn
    },
    addObj = function(thing)
    {
      assign(.jcall(thing$obj,"Ljava/lang/String;","getUniqId"),thing,env)
    },
    getObj = function(javaobj)
    {
      if(is.null(javaobj))
        return(NULL)
      iri <- .jcall(javaobj,"Ljava/lang/String;","getUniqId")
      if(exists(iri,env))
      {
        return(get(iri,env))
      }
      classiri <- .jcall(javaobj,"Ljava/lang/String;","getClassTypeIri")
      if(!exists(classiri,TypeMap))
      {
        stop(paste("class type iri not found:"))
      }
      clazz <- get(classiri,TypeMap)
      #object will register itself automaticly
      return(clazz$new(.self,iri))      
    }
  )
)


Domain<- setRefClass(
  "Domain",
  fields = list(
    domain = "ANY",
    env = "ANY",
%1$s
  ),
  methods = list(
    initialize = function(config)
    {
      domain <<- .jnew("nl/wur/ssb/RDFSimpleCon/api/Domain",config)
      inner <- DomainInner$new(domain)
%2$s
    },
    close = function()
    {
      # .jcall(exporter,"I","export",paste(dir,"/",file,".xgmml",sep=""),name,net,geneNames,geneNames)
      .jcall(domain,"V","close")
    },
    save = function(fileName)
    {
      # .jcall(exporter,"I","export",paste(dir,"/",file,".xgmml",sep=""),name,net,geneNames,geneNames)
      .jcall(domain,"V","save",fileName)
    }
  )
)

OWLThing<- setRefClass(
  "OWLThing",
  fields = list(
    obj = "ANY",
    domain = "ANY"
  )
)

decodeInt <- function(inVal)
{
  if(is.null(inVal))
    return(NaN)
  .jcall(inVal,"I","intValue")
}

decodeLong <- function(inVal)
{
  if(is.null(inVal))
    return(NaN)
  .jcall(inVal,"J","intValue")
}
